/*
 * ethernet_lwip.h
 *
 *  Created on: Sep 14, 2021
 *      Author: Zeder
 */

#ifndef ETHERNET_LWIP_ETHERNET_LWIP_H_
#define ETHERNET_LWIP_ETHERNET_LWIP_H_

#include "lwip/debug.h"
#include "lwip/stats.h"
#include "lwip/tcp.h"

#define CTT_SERVER_IP1  18
#define CTT_SERVER_IP2  138
#define CTT_SERVER_IP3  185
#define CTT_SERVER_IP4  90
#define CTT_SERVER_PORT	80
typedef enum{
	LWIP_IDLE = 10,
	LWIP_ERROR,
	LWIP_CHECK,
	LWIP_TRANS_SYSINFO,
	LWIP_TRANS_CLI,
	LWIP_TRANS_IRRI,
//	LWIP_TRANS_RECEIVE_CHECK,
//	LWIP_CHECK_RECEIVE,
	LWIP_PARAM_SAVE
} EthState_t;
typedef struct{
	char* data;
	EthState_t state;
} EthernetPacket_t;
void lwip_ethernet_process(EthernetPacket_t EthState_);
void lwip_timer(void);
#endif /* ETHERNET_LWIP_ETHERNET_LWIP_H_ */
