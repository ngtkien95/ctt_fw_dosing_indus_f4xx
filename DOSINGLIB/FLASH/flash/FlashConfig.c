/*
 * FlashConfig.c
 *
 *  Created on: Aug 13, 2021
 *      Author: Zeder
 */
#define FLASH_USER_START_ADDR   ADDR_FLASH_SECTOR_8   /* Start @ of user Flash area */
#define FLASH_USER_END_ADDR     ADDR_FLASH_SECTOR_11  +  GetSectorSize(ADDR_FLASH_SECTOR_11) -1 /* End @ of user Flash area : sector start address + sector size -1 */


/**
  * @brief  Gets the sector of a given address
  * @param  None
  * @retval The sector of a given address
  */
#include "FlashConfig.h"
bool TestFlash(void){
	HAL_StatusTypeDef status;

	status = FlashEraseSector(ADDR_FLASH_SECTOR_8,FLASH_USER_END_ADDR);

    status = HAL_FLASH_Unlock();

    FlashWriteWord(ADDR_FLASH_SECTOR_8,(uint32_t)0x97375678);
    status = HAL_FLASH_Lock();

    if(HAL_OK == status){
    	return true;
    }
    else {
    	return false;
    }

}
uint32_t GetSector(uint32_t Address)
{
	uint32_t sector = 0;

	if((Address < ADDR_FLASH_SECTOR_1) && (Address >= ADDR_FLASH_SECTOR_0))
	{
		sector = FLASH_SECTOR_0;
	}
	else if((Address < ADDR_FLASH_SECTOR_2) && (Address >= ADDR_FLASH_SECTOR_1))
	{
		sector = FLASH_SECTOR_1;
	}
	else if((Address < ADDR_FLASH_SECTOR_3) && (Address >= ADDR_FLASH_SECTOR_2))
	{
		sector = FLASH_SECTOR_2;
	}
	else if((Address < ADDR_FLASH_SECTOR_4) && (Address >= ADDR_FLASH_SECTOR_3))
	{
		sector = FLASH_SECTOR_3;
	}
	else if((Address < ADDR_FLASH_SECTOR_5) && (Address >= ADDR_FLASH_SECTOR_4))
	{
		sector = FLASH_SECTOR_4;
	}
	else if((Address < ADDR_FLASH_SECTOR_6) && (Address >= ADDR_FLASH_SECTOR_5))
	{
		sector = FLASH_SECTOR_5;
	}
	else if((Address < ADDR_FLASH_SECTOR_7) && (Address >= ADDR_FLASH_SECTOR_6))
	{
		sector = FLASH_SECTOR_6;
	}
	else if((Address < ADDR_FLASH_SECTOR_8) && (Address >= ADDR_FLASH_SECTOR_7))
	{
		sector = FLASH_SECTOR_7;
	}
	else if((Address < ADDR_FLASH_SECTOR_9) && (Address >= ADDR_FLASH_SECTOR_8))
	{
		sector = FLASH_SECTOR_8;
	}
	else if((Address < ADDR_FLASH_SECTOR_10) && (Address >= ADDR_FLASH_SECTOR_9))
	{
		sector = FLASH_SECTOR_9;
	}
	else if((Address < ADDR_FLASH_SECTOR_11) && (Address >= ADDR_FLASH_SECTOR_10))
	{
		sector = FLASH_SECTOR_10;
	}
	else /* (Address < FLASH_END_ADDR) && (Address >= ADDR_FLASH_SECTOR_11) */
			{
		sector = FLASH_SECTOR_11;
			}

	return sector;
}

uint32_t GetSectorSize(uint32_t Sector)
{
	uint32_t sectorsize = 0x00;

	if((Sector == FLASH_SECTOR_0) || (Sector == FLASH_SECTOR_1) || (Sector == FLASH_SECTOR_2) || (Sector == FLASH_SECTOR_3))
	{
		sectorsize = 16 * 1024;
	}
	else if(Sector == FLASH_SECTOR_4)
	{
		sectorsize = 64 * 1024;
	}
	else
	{
		sectorsize = 128 * 1024;
	}
	return sectorsize;
}
//Erase from Start Sector to End Sector
bool FlashEraseSector(uint32_t StartSector, uint32_t EndSector)
{
	uint32_t FirstSector = 0, NbOfSectors = 0;
	uint32_t SECTORError = 0;
	static FLASH_EraseInitTypeDef EraseInitStruct;
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

	FirstSector = GetSector(StartSector);
	/* Get the number of sector to erase from 1st sector*/
	NbOfSectors = GetSector(EndSector) - FirstSector + 1;
	/* Fill EraseInit structure*/
	EraseInitStruct.TypeErase     = FLASH_TYPEERASE_SECTORS;
	EraseInitStruct.VoltageRange  = FLASH_VOLTAGE_RANGE_3;
	EraseInitStruct.Sector        = FirstSector;
	EraseInitStruct.NbSectors     = NbOfSectors;

	/* Unlock the Flash to enable the flash control register access *************/
	//HAL_FLASH_Unlock();
	/* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
	     you have to make sure that these data are rewritten before they are accessed during code
	     execution. If this cannot be done safely, it is recommended to flush the caches by setting the
	     DCRST and ICRST bits in the FLASH_CR register. */
	if (HAL_FLASHEx_Erase(&EraseInitStruct, &SECTORError) != HAL_OK){
		Error_Handler();
	}
	/* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
	     you have to make sure that these data are rewritten before they are accessed during code
	     execution. If this cannot be done safely, it is recommended to flush the caches by setting the
	     DCRST and ICRST bits in the FLASH_CR register. */
	__HAL_FLASH_DATA_CACHE_DISABLE();
	__HAL_FLASH_INSTRUCTION_CACHE_DISABLE();

	__HAL_FLASH_DATA_CACHE_RESET();
	__HAL_FLASH_INSTRUCTION_CACHE_RESET();

	__HAL_FLASH_INSTRUCTION_CACHE_ENABLE();
	__HAL_FLASH_DATA_CACHE_ENABLE();

	//HAL_FLASH_Lock();

	return true;

}

bool FlashWriteWord(uint32_t Address, uint32_t data){
	/* Unlock the Flash to enable the flash control register access *************/
    //HAL_FLASH_Unlock();

    if(HAL_OK == HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, Address, data)){
         return true;
    }
    else {
    	return false;
    }
}
bool FlashWriteString(uint32_t adr, uint32_t * data)
{
	uint8_t index = 0;
	uint8_t NbWords = 0;

	NbWords = (strlen((const char *)data)/4) + ((strlen((const char *)data) % 4) != 0);

	while (index<NbWords)
	{
		FlashWriteWord(adr,data[index]);
		adr += 4;
		index++;
	}
	return true;
}
