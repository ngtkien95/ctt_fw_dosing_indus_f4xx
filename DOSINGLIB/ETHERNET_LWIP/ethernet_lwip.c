
#include "../include.h"
#include "ethernet_lwip.h"
#include "tcp_client.h"

char PostMethod[] = "GET ";
char EndPacket[] = " HTTP/1.1\r\nHost: 18.138.185.90\r\n\r\n";
static struct tcp_pcb *ctt_pcb_client; //client pcb
static ip_addr_t ctt_server_addr; //server ip
extern _SENSOR_IRRI tSensorIrriValue;
extern _SENSOR_CLI tSensorCliValue;
extern _HCSR04 tDistance;
extern _FAM_INFO tFamInfo;
char BEGIN_STR[] = "GET ";
char END_STR[] = " HTTP/1.1\r\nHost: 18.138.185.90\r\n\r\n";
char findPayload[] = "{\"success\":";
/* extern value */
extern int32_t nTimeOnPumpPeriEc;
extern int32_t nTimeOnPumpPeriPh;
struct LWIP_Packet_t
{
  char contents[2048]; //you may add more information
};//1024 bytes
EthernetPacket_t EthState_;
struct LWIP_Packet_t data_packet; //2048 bytes time_packet structure
uint16_t nReadBuffer = 0; //read buffer index
uint16_t nWrittenBuffer = 0; //write buffer index
char* test_data1 = "POST /getSensorVal HTTP/1.1\r\nHost: 18.138.185.90:80\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 15\r\n\r\npCode=SGN000000IN000000";
char* test_data_d1 = "data time";

static struct LWIP_Packet_t LWIP_StringSysInfo(void);
static struct LWIP_Packet_t LWIP_StringClimate(void);
static struct  LWIP_Packet_t LWIP_StringIrrigation(void);
/* callback functions */
static err_t lwip_tcp_callback_connected(void *arg, struct tcp_pcb *pcb_new, err_t err);
static err_t lwip_tcp_callback_sent(void *arg, struct tcp_pcb *tpcb, u16_t len);
static err_t lwip_tcp_callback_received(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
static err_t lwip_tcp_callback_poll(void *arg, struct tcp_pcb *tpcb);
static void lwip_tcp_callback_error(void *arg, err_t err);

/* functions */
static void lwip_open_conn(void); //open function

static void lwip_send_data(void); //send function
static void lwip_close_conn();
void lwip_ethernet_process(EthernetPacket_t EthPack_){

	//TODO: call from lwip_timer
	switch(EthPack_.state){
	case LWIP_IDLE:
		break;
	case LWIP_CHECK:
		break;
	case LWIP_TRANS_SYSINFO:
		app_open_conn();
		break;
	case LWIP_TRANS_CLI:
		app_open_conn();
		break;
	case LWIP_TRANS_IRRI:
		app_open_conn();
		break;
	case LWIP_PARAM_SAVE:

		break;
	case LWIP_ERROR:
		break;
	}
}

/*
 * app_open_connection
 * create a client pcb & call tcp_connect
 */
static void lwip_open_conn(void)
{
  err_t err;

  if (ctt_pcb_client == NULL)
  {
	  ctt_pcb_client = tcp_new();
    if (ctt_pcb_client == NULL) //lack of memory
    {
      memp_free(MEMP_TCP_PCB, ctt_pcb_client);
      ctt_pcb_client = NULL;

    }
  }

  IP4_ADDR(&ctt_server_addr, CTT_SERVER_IP1, CTT_SERVER_IP2, CTT_SERVER_IP3, CTT_SERVER_IP4); //server ip
  err = tcp_connect(ctt_pcb_client, &ctt_server_addr, CTT_SERVER_PORT, lwip_tcp_callback_connected); //connect


  if(err == ERR_ISCONN) //already connected
  {
	  lwip_close_conn();
	  return;
  }

  //After send data - close connection
  lwip_close_conn();
}

/*
 * tcp_callback_connected
 * callback when connected, client sends a request to the server
 */
static err_t lwip_tcp_callback_connected(void *arg, struct tcp_pcb *pcb_new, err_t err)
{
  LWIP_UNUSED_ARG(arg);

  if (err != ERR_OK) //error when connect to the server
  {
    return err;
  }

  tcp_setprio(pcb_new, TCP_PRIO_NORMAL); //set priority for the client pcb


  //
  tcp_arg(pcb_new, 0); //no argument is used
  tcp_sent(pcb_new, lwip_tcp_callback_sent); //register send callback
  tcp_recv(pcb_new, lwip_tcp_callback_received);  //register receive callback
  tcp_err(pcb_new, lwip_tcp_callback_error); //register error callback
  tcp_poll(pcb_new, lwip_tcp_callback_poll, 0); //register poll callback

  //lwip_send_data(); //send a request

  return ERR_OK;
}

/*
 * app_send_data
 * send the request to the server
 */
static void lwip_send_data(void)
{
  memset(&data_packet, 0, sizeof(struct LWIP_Packet_t));
//  data_packet.head = 0xAE; //head
//  data_packet.type = REQ; //request type
//  data_packet.tail = 0xEA; //tail
  sprintf((char*)data_packet.contents,"%s",EthState_.data);
  nWrittenBuffer = 0; //clear index
  switch(EthState_.state){
  case LWIP_TRANS_SYSINFO:
	  sprintf((char*)data_packet.contents,"%s",EthState_.data);
	  break;
  case LWIP_TRANS_CLI:
	  sprintf((char*)data_packet.contents,"%s",EthState_.data);
	  break;
  case LWIP_TRANS_IRRI:
	  sprintf((char*)data_packet.contents,"%s",EthState_.data);
	  break;

  }

  tcp_write(ctt_pcb_client, &data_packet,sizeof(struct LWIP_Packet_t), 0); //use pointer, should not changed until receive ACK

}

/*
 * tcp_callback_sent
 * callback when data sending is finished, control leds
 */
static err_t lwip_tcp_callback_sent(void *arg, struct tcp_pcb *tpcb, u16_t len)
{
  LWIP_UNUSED_ARG(arg);
  LWIP_UNUSED_ARG(tpcb);
  LWIP_UNUSED_ARG(len);

  nWrittenBuffer += len;

  if(nWrittenBuffer < sizeof(struct LWIP_Packet_t)) //need to flush remain data
  {
    tcp_output(ctt_pcb_client); //flush
  }
  else if(nWrittenBuffer > sizeof(struct LWIP_Packet_t)) //invalid length of sent data
  {

    lwip_close_conn();
  }
  else
  {

//    HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin); //blink green when sent O.K
  }

  return ERR_OK;
}

/*
 * tcp_callback_received
 * callback when data is received, validate received data and parse it
 */
static err_t lwip_tcp_callback_received(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err)
{
  err_t ret_err;

  if (p == NULL) //pbuf is null when session is closed
  {
	lwip_close_conn();
    ret_err = ERR_OK;
  }
  else if (err != ERR_OK) //ERR_ABRT is returned when called tcp_abort
  {
    tcp_recved(tpcb, p->tot_len); //advertise window size

    pbuf_free(p); //free pbuf
    ret_err = err;
  }
  else //receiving data
  {
    tcp_recved(tpcb, p->tot_len); //advertise window size

    memcpy(&data_packet + nReadBuffer, p->payload, p->len);
    nReadBuffer += p->len;

//    if(nReadBuffer == sizeof(struct time_packet) && packet.type == RESP) //if received length is valid
//    {
//      nReadBuffer = 0;
//
//      printf("%04d-%02d-%02d %02d:%02d:%02d\n",
//             packet.year + 2000,
//             packet.month, packet.day, packet.hour, packet.minute, packet.second); //print time information
//
//      app_close_conn(); //close connection
//    }
//    else if(nReadBuffer > sizeof(struct time_packet))
//    {
//      nReadBuffer = 0;
//      app_close_conn(); //close connection
//    }
    nReadBuffer = 0;
    lwip_close_conn(); //close connection

    pbuf_free(p); //free pbuf
    ret_err = ERR_OK;
  }

  return ret_err;
}

/*
 * app_close_conn
 * close connection & clear callbacks
 */
static void lwip_close_conn()
{
  /* clear callback functions */
  tcp_arg(ctt_pcb_client, NULL);
  tcp_sent(ctt_pcb_client, NULL);
  tcp_recv(ctt_pcb_client, NULL);
  tcp_err(ctt_pcb_client, NULL);
  tcp_poll(ctt_pcb_client, NULL, 0);

  tcp_close(ctt_pcb_client);    //close connection
  ctt_pcb_client = NULL;


}

/*
 *  error callback
 *  call when there's an error, turn on an error led
 */
static void lwip_tcp_callback_error(void *arg, err_t err)
{
  LWIP_UNUSED_ARG(arg);
  LWIP_UNUSED_ARG(err);

//  HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, GPIO_PIN_SET); //error loed
}

/*
 * poll callback
 * called when lwip is idle, do something such as watchdog reset
 */
static err_t lwip_tcp_callback_poll(void *arg, struct tcp_pcb *tpcb)
{
  return ERR_OK;
}

void lwip_timer(void){

}




struct LWIP_Packet_t LWIP_StringSysInfo(void)
{

	struct LWIP_Packet_t packet;

	char data[100];

	sprintf((char *)packet.contents, "%s", BEGIN_STR);

	sprintf((char *)data, "/sysinfoIN?FarmCode=%s", tFamInfo.cFarmCode);
	strcat(packet.contents, data);
	sprintf((char *)data, "&T_Update=%04d", (char*)tFamInfo.nTimeUpload);
	strcat(packet.contents, data);
	sprintf((char *)data, "&Thre_EC=%04d", tFamInfo.nThrehold_Ec);
	strcat(packet.contents, data);
	sprintf((char *)data, "&ECLo=%04d", tFamInfo.nThrehold_EcLow);
	strcat(packet.contents, data);
	sprintf((char *)data, "&ECHi=%04d", tFamInfo.nThrehold_ECHigh);
	strcat(packet.contents, data);
	sprintf((char *)data, "&ECDual=%04d", tFamInfo.nThrehold_ECDual);
	strcat(packet.contents, data);
	sprintf((char *)data, "&Thre_WT=%04d", tFamInfo.nThrehold_WT);
	strcat(packet.contents, data);
	sprintf((char *)data, "&WTLo=%04d", tFamInfo.nThrehold_WTLow);
	strcat(packet.contents, data);
	sprintf((char *)data, "&WTHi=%04d", tFamInfo.nThrehold_WTHigh);
	strcat(packet.contents, data);
	sprintf((char *)data, "&WTDual=%04d", tFamInfo.nThrehold_WTDual);
	strcat(packet.contents, data);
	sprintf((char *)data, "&Thre_PH=%04d", tFamInfo.nThrehold_PH);
	strcat(packet.contents, data);
	sprintf((char *)data, "&PHLo=%04d", tFamInfo.nThrehold_PHLow);
	strcat(packet.contents, data);
	sprintf((char *)data, "&PHHi=%04d", tFamInfo.nThrehold_PHHigh);
	strcat(packet.contents, data);
	sprintf((char *)data, "&PHDual=%04d", tFamInfo.nThrehold_PHDual);
	strcat(packet.contents, data);
	sprintf((char *)data, "&Thre_Temp=%04d", tFamInfo.nThrehold_Cli_Temp);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TempLo=%04d", tFamInfo.nThrehold_Cli_TempLow);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TempHi=%04d", tFamInfo.nThrehold_Cli_TempHigh);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TempDual=%04d", tFamInfo.nThrehold_Cli_TempDual);
	strcat(packet.contents, data);
	sprintf((char *)data, "&Thre_Humi=%04d", tFamInfo.nThrehold_Cli_Humi);
	strcat(packet.contents, data);
	sprintf((char *)data, "&HumiLo=%04d", tFamInfo.nThrehold_Cli_HumiLow);
	strcat(packet.contents, data);
	sprintf((char *)data, "&HumiHi=%04d", tFamInfo.nThrehold_Cli_HumiHigh);
	strcat(packet.contents, data);
	sprintf((char *)data, "&HumiDual=%04d", tFamInfo.nThrehold_Cli_HumiDual);
	strcat(packet.contents, data);
	sprintf((char *)data, "&Thre_Light=%05d", tFamInfo.nThrehold_Cli_Light);
	strcat(packet.contents, data);
	sprintf((char *)data, "&LightLo=%05d", tFamInfo.nThrehold_Cli_LightLow);
	strcat(packet.contents, data);
	sprintf((char *)data, "&LightHi=%05d", tFamInfo.nThrehold_Cli_LightHigh);
	strcat(packet.contents, data);
	sprintf((char *)data, "&LightDual=%05d", tFamInfo.nThrehold_Cli_LightDual);
	strcat(packet.contents, data);
	sprintf((char *)data, "&Thre_Flow=%04d", tFamInfo.nThrehold_FlowRate);
	strcat(packet.contents, data);
	sprintf((char *)data, "&FlowLo=%04d", tFamInfo.nThrehold_FlowRateLow);
	strcat(packet.contents, data);
	sprintf((char *)data, "&FlowHi=%04d", tFamInfo.nThrehold_FlowRateHigh);
	strcat(packet.contents, data);
	sprintf((char *)data, "&FlowDual=%04d", tFamInfo.nThrehold_FlowRateDual);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TankLv=%04d", tFamInfo.nThrehold_TankLevel);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TankLvLo=%04d", tFamInfo.nThrehold_TankLevelLow);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TankLvHi=%04d", tFamInfo.nThrehold_TankLevelHigh);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TankLvDual=%04d", tFamInfo.nThrehold_TankLevelDual);
	strcat(packet.contents, data);
	sprintf((char *)data, "&T_Peri=%04d", tFamInfo.nPump_TimePeri);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TNozOn=%04d", tFamInfo.nPump_TimeNozzleOn);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TNozOff=%04d", tFamInfo.nPump_TimeNozzleOff);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TPumpMix=%04d", tFamInfo.nPump_TimePumpMix);
	strcat(packet.contents, data);
	sprintf((char *)data, "&EC1A=%04d", tFamInfo.nCalib_Ec1A);
	strcat(packet.contents, data);
	sprintf((char *)data, "&EC1B=%04d", tFamInfo.nCalib_Ec1B);
	strcat(packet.contents, data);
	sprintf((char *)data, "&EC2A=%04d", tFamInfo.nCalib_Ec2A);
	strcat(packet.contents, data);
	sprintf((char *)data, "&EC2B=%04d", tFamInfo.nCalib_Ec2B);
	strcat(packet.contents, data);
	sprintf((char *)data, "&WT1A=%04d", tFamInfo.nCalib_Wt1A);
	strcat(packet.contents, data);
	sprintf((char *)data, "&WT1B=%04d", tFamInfo.nCalib_Wt1B);
	strcat(packet.contents, data);
	sprintf((char *)data, "&WT2A=%04d", tFamInfo.nCalib_Wt2A);
	strcat(packet.contents, data);
	sprintf((char *)data, "&WT2B=%04d", tFamInfo.nCalib_Wt2B);
	strcat(packet.contents, data);
	sprintf((char *)data, "&PH1A=%04d", tFamInfo.nCalib_Ph1A);
	strcat(packet.contents, data);
	sprintf((char *)data, "&PH1B=%04d", tFamInfo.nCalib_Ph1B);
	strcat(packet.contents, data);
	sprintf((char *)data, "&PH2A=%04d", tFamInfo.nCalib_Ph2A);
	strcat(packet.contents, data);
	sprintf((char *)data, "&PH2B=%04d", tFamInfo.nCalib_Ph2B);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_TE1=%04d", tFamInfo.nOffset_Cli_Temp1);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_TE2=%04d", tFamInfo.nOffset_Cli_Temp2);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_HU1=%04d", tFamInfo.nOffset_Cli_Humi1);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_HU2=%04d", tFamInfo.nOffset_Cli_Humi2);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_LI1=%04d", tFamInfo.nOffset_Cli_Light1);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_LI2=%04d", tFamInfo.nOffset_Cli_Light2);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_US1=%04d", tFamInfo.nOffset_US1);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_US2=%04d", tFamInfo.nOffset_US2);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_US3=%04d", tFamInfo.nOffset_US3);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_US4=%04d", tFamInfo.nOffset_US4);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_US5=%04d", tFamInfo.nOffset_US5);
	strcat(packet.contents, data);
	sprintf((char *)data, "&O_US6=%04d", tFamInfo.nOffset_US6);
	strcat(packet.contents, data);
	sprintf((char *)data, "&VolN_PH1=%04d", tFamInfo.nVoltage_Ph1_Newtral);
	strcat(packet.contents, data);
	sprintf((char *)data, "&VolN_PH2=%04d", tFamInfo.nVoltage_Ph2_Newtral);
	strcat(packet.contents, data);
	sprintf((char *)data, "&VolA_PH1=%04d", tFamInfo.nVoltage_Ph1_Acid);
	strcat(packet.contents, data);
	sprintf((char *)data, "&VolA_PH2=%04d", tFamInfo.nVoltage_Ph2_Acid);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TNozOn=%04d", tFamInfo.nTime_NozzleOn);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TNozOff=%04d", tFamInfo.nTime_NozzleOff);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TCooOn=%04d", tFamInfo.nTime_CoopadOn);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TCooOff=%04d", tFamInfo.nTime_CoopadOff);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TChiOn=%04d", tFamInfo.nTime_ChillerOn);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TChiOff=%04d", tFamInfo.nTime_ChillerOff);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TNetOn=%04d", tFamInfo.nTime_ShadingNetOn);
	strcat(packet.contents, data);
	sprintf((char *)data, "&TNetOff=%04d", tFamInfo.nTime_ShadingNetOff);
	strcat(packet.contents, data);
	sprintf((char *)data, "&FWVer=%s", tFamInfo.cFwVersion);
	strcat(packet.contents, data);

	strcat(packet.contents, END_STR);


	return packet;
}
struct LWIP_Packet_t LWIP_StringClimate(void)
{
	struct LWIP_Packet_t packet;

	char data[100];

	sprintf(packet.contents, "%s", BEGIN_STR);

	sprintf((char *)data, "/cliCollect?farmCode=%s", tFamInfo.cFarmCode);
	strcat(packet.contents, data);

	switch (tSensorCliValue.tTempCheck)
	{
	case CHECK_TEMP_OK:
		sprintf(data, "&cliTemp1=%2.2f&cliTemp2=%2.2f&cliTempe=%s", tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2, "OK");
		break;
	case CHECK_TEMP_DUAL:
		sprintf(data, "&cliTemp1=%2.2f&cliTemp2=%2.2f&cliTempe=%s", tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2, "DUAL");
		break;
	case CHECK_TEMP_HIGH:
		sprintf(data, "&cliTemp1=%2.2f&cliTemp2=%2.2f&cliTempe=%s", tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2, "HIGH");
		break;
	case CHECK_TEMP_LOW:
		sprintf(data, "&cliTemp1=%2.2f&cliTemp2=%2.2f&cliTempe=%s", tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2, "LOW");
		break;
	}
	strcat(packet.contents, data);

	switch (tSensorCliValue.tHumiCheck)
	{
	case CHECK_HUMI_OK:
		sprintf(data, "&cliHumi1=%2.2f&cliHumi2=%2.2f&cliHumie=%s", tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2, "OK");
		break;
	case CHECK_HUMI_DUAL:
		sprintf(data, "&cliHumi1=%2.2f&cliHumi2=%2.2f&cliHumie=%s", tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2, "DUAL");
		break;
	case CHECK_HUMI_HIGH:
		sprintf(data, "&cliHumi1=%2.2f&cliHumi2=%2.2f&cliHumie=%s", tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2, "HIGH");
		break;
	case CHECK_HUMI_LOW:
		sprintf(data, "&cliHumi1=%2.2f&cliHumi2=%2.2f&cliHumie=%s", tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2, "LOW");
		break;
	}
	strcat(packet.contents, data);

	switch (tSensorCliValue.tLightCheck)
	{
	case CHECK_LIGHT_OK:
		sprintf(data, "&cliLight1=%d&cliLight2=%d&cliLighte=%s", tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2, "OK");
		break;
	case CHECK_LIGHT_DUAL:
		sprintf(data, "&cliLight1=%d&cliLight2=%d&cliLighte=%s", tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2, "DUAL");
		break;
	case CHECK_LIGHT_HIGH:
		sprintf(data, "&cliLight1=%d&cliLight2=%d&cliLighte=%s", tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2, "HIGH");
		break;
	case CHECK_LIGHT_LOW:
		sprintf(data, "&cliLight1=%d&cliLight2=%d&cliLighte=%s", tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2, "LOW");
		break;
	}
	strcat(packet.contents, data);

	sprintf(data, "&cliSttFan=%s", "UNKNOW");
	strcat(packet.contents, data);

	sprintf(data, "&cliSttCool=%s", "UNKNOW");
	strcat(packet.contents, data);

	sprintf(data, "&cliSttNozzle=%s", "UNKNOW");
	strcat(packet.contents, data);

	sprintf(data, "&cliSttShading=%s", "UNKNOW");
	strcat(packet.contents, data);

	sprintf(data, "&firmVersion=%s", tFamInfo.cFwVersion);
	strcat(packet.contents, data);

	strcat(packet.contents, END_STR);

	// 18.138.185.90/cliCollect?farmCode=SGN022004IN000789&cliTemp1=33.45&cliTemp2=34.33&cliTempe=OK&cliHumi1=50.45&cliHumi2=50.66&cliHumie=LOW&cliLight1=12000&cliLight2=13000&cliLighte=HIGH&cliSttFan=UNKNOW&cliSttCool=UNKNOW&cliSttNozzle=UNKNOW&cliSttShading=UNKNOW&firmVersion=IN01A






	return packet;
}

struct LWIP_Packet_t LWIP_StringIrrigation(void){

	struct LWIP_Packet_t packet;

	char data[100];

	sprintf(packet.contents, "%s", BEGIN_STR);

	sprintf(data, "/irrCollect2?farmCode=%s", tFamInfo.cFarmCode);
	strcat(packet.contents, data);

	switch (tSensorIrriValue.tPhCheck)
	{
	case CHECK_PH_OK:
		sprintf(data, "&irrPH1=%1.2f&irrPH2=%1.2f&irrPHe=%s", tSensorIrriValue.dPh1, tSensorIrriValue.dPh2, "OK");
		break;
	case CHECK_PH_HIGH:
		sprintf(data, "&irrPH1=%1.2f&irrPH2=%1.2f&irrPHe=%s", tSensorIrriValue.dPh1, tSensorIrriValue.dPh2, "HIGH");
		break;
	case CHECK_PH_LOW:
		sprintf(data, "&irrPH1=%1.2f&irrPH2=%1.2f&irrPHe=%s", tSensorIrriValue.dPh1, tSensorIrriValue.dPh2, "LOW");
		break;
	case CHECK_PH_DUAL:
		sprintf(data, "&irrPH1=%1.2f&irrPH2=%1.2f&irrPHe=%s", tSensorIrriValue.dPh1, tSensorIrriValue.dPh2, "DUAL");
		break;
	}
	strcat(packet.contents, data);

	switch (tSensorIrriValue.tEcCheck)
	{
	case CHECK_EC_OK:
		sprintf(data, "&irrEC1=%1.2f&irrEC2=%1.2f&irrECe=%s", tSensorIrriValue.dEc1, tSensorIrriValue.dEc2, "OK");
		break;
	case CHECK_EC_HIGH:
		sprintf(data, "&irrEC1=%1.2f&irrEC2=%1.2f&irrECe=%s", tSensorIrriValue.dEc1, tSensorIrriValue.dEc2, "HIGH");
		break;
	case CHECK_EC_LOW:
		sprintf(data, "&irrEC1=%1.2f&irrEC2=%1.2f&irrECe=%s", tSensorIrriValue.dEc1, tSensorIrriValue.dEc2, "LOW");
		break;
	case CHECK_EC_DUAL:
		sprintf(data, "&irrEC1=%1.2f&irrEC2=%1.2f&irrECe=%s", tSensorIrriValue.dEc1, tSensorIrriValue.dEc2, "DUAL");
		break;
	}
	strcat(packet.contents, data);

	switch (tSensorIrriValue.tWtCheck)
	{
	case CHECK_WT_OK:
		sprintf(data, "&irrWaterTemp1=%2.2f&irrWaterTemp2=%2.2f&irrWaterTempe=%s", tSensorIrriValue.dWaterTemp1, tSensorIrriValue.dWaterTemp2, "OK");
		break;
	case CHECK_WT_HIGH:
		sprintf(data, "&irrWaterTemp1=%2.2f&irrWaterTemp2=%2.2f&irrWaterTempe=%s", tSensorIrriValue.dWaterTemp1, tSensorIrriValue.dWaterTemp2, "HIGH");
		break;
	case CHECK_WT_LOW:
		sprintf(data, "&irrWaterTemp1=%2.2f&irrWaterTemp2=%2.2f&irrWaterTempe=%s", tSensorIrriValue.dWaterTemp1, tSensorIrriValue.dWaterTemp2, "LOW");
		break;
	case CHECK_WT_DUAL:
		sprintf(data, "&irrWaterTemp1=%2.2f&irrWaterTemp2=%2.2f&irrWaterTempe=%s", tSensorIrriValue.dWaterTemp1, tSensorIrriValue.dWaterTemp2, "DUAL");
		break;
	}
	strcat(packet.contents, data);

	switch (tDistance.tTank1Check)
	{
	case CHECK_TANK1_HIGH:
		sprintf(data, "&irrF1A=%03d&irrF1B=%03d&irrF1E=%s", tDistance.dLitUs1, tDistance.dLitUs2, "HIGH");
		break;
	case CHECK_TANK1_LOW:
		sprintf(data, "&irrF1A=%03d&irrF1B=%03d&irrF1E=%s", tDistance.dLitUs1, tDistance.dLitUs2, "LOW");
		break;
	case CHECK_TANK1_DUAL:
		sprintf(data, "&irrF1A=%03d&irrF1B=%03d&irrF1E=%s", tDistance.dLitUs1, tDistance.dLitUs2, "DUAL");
		break;
	case CHECK_TANK1_OK:
		sprintf(data, "&irrF1A=%03d&irrF1B=%03d&irrF1E=%s", tDistance.dLitUs1, tDistance.dLitUs2, "OK");
		break;
	}
	//sprintf(data,"&irrF1A=80&irrF1B=81&irrF2E=OK");
	strcat(packet.contents, data);

	switch (tDistance.tTank2Check)
	{
	case CHECK_TANK2_HIGH:
		sprintf(data, "&irrF2A=%03d&irrF2B=%03d&irrF2E=%s", tDistance.dLitUs3, tDistance.dLitUs4, "HIGH");
		break;
	case CHECK_TANK2_LOW:
		sprintf(data, "&irrF2A=%03d&irrF2B=%03d&irrF2E=%s", tDistance.dLitUs3, tDistance.dLitUs4, "LOW");
		break;
	case CHECK_TANK2_DUAL:
		sprintf(data, "&irrF2A=%03d&irrF2B=%03d&irrF2E=%s", tDistance.dLitUs3, tDistance.dLitUs4, "DUAL");
		break;
	case CHECK_TANK2_OK:
		sprintf(data, "&irrF2A=%03d&irrF2B=%03d&irrF2E=%s", tDistance.dLitUs3, tDistance.dLitUs4, "OK");
		break;
	}
	//sprintf(data,"&irrF2A=137&irrF2B=137&irrF2E=HIGH");
	strcat(packet.contents, data);

	switch (tDistance.tTank3Check)
	{
	case CHECK_TANK3_HIGH:
		sprintf(data, "&irrF3A=%03d&irrF3B=%03d&irrF3E=%s", tDistance.dLitUs5, tDistance.dLitUs6, "HIGH");
		break;
	case CHECK_TANK3_LOW:
		sprintf(data, "&irrF3A=%03d&irrF3B=%03d&irrF3E=%s", tDistance.dLitUs5, tDistance.dLitUs6, "LOW");
		break;
	case CHECK_TANK3_DUAL:
		sprintf(data, "&irrF3A=%03d&irrF3B=%03d&irrF3E=%s", tDistance.dLitUs5, tDistance.dLitUs6, "DUAL");
		break;
	case CHECK_TANK3_OK:
		sprintf(data, "&irrF3A=%03d&irrF3B=%03d&irrF3E=%s", tDistance.dLitUs5, tDistance.dLitUs6, "OK");
		break;
	}
	//sprintf(data,"&irrF3A=137&irrF3B=137&irrF2E=HIGH");
	strcat(packet.contents, data);

	sprintf(data, "&irrFlow1=%d&irrFlow2=%d&irrFlowe=%s", 0, 0, "UNKNOW");
	strcat(packet.contents, data);

	if (nTimeOnPumpPeriPh != 0)
	{
		sprintf(data, "&irrSttPumpPh=%s", "OPEN");
	}
	else
	{
		sprintf(data, "&irrSttPumpPh=%s", "CLOSE");
	}
	strcat(packet.contents, data);

	if (nTimeOnPumpPeriEc != 0)
	{
		sprintf(data, "&irrSttPumpEc=%s", "OPEN");
	}
	else
	{
		sprintf(data, "&irrSttPumpEc=%s", "CLOSE");
	}
	strcat(packet.contents, data);

	sprintf(data, "&irrSttNutriPump=%s", "UNKNOW");
	strcat(packet.contents, data);

	sprintf(data, "&irrSttChillerPump=%s", "UNKNOW");
	strcat(packet.contents, data);

	sprintf(data, "&irStSol=%s", "00");
	strcat(packet.contents, data);

	sprintf(data, "&firmVersion=%s", tFamInfo.cFwVersion);
	strcat(packet.contents, data);


	strcat(packet.contents, END_STR);


	return packet;
}
