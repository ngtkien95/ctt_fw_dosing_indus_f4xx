/*
 * pump.c
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#include "../include.h"

//TODO: Ethernet Exclude
//extern EthState_t EthState;
extern modeSetting_t modeSetting;

extern _SENSOR_IRRI tSensorIrriValue;
extern _FAM_INFO tFamInfo;

sttControlPump_t sttControlPump;
int16_t nPumpPeriod = 0;
int32_t nTimeOnMotorMix = 0;
int32_t nTimeOnPumpPeriEc = 0;
int32_t nTimeOnPumpPeriPh = 0;
int8_t nTimeCheckEc = 0;
uint16_t nThresholdPumpEcLow = 0;
uint16_t nThresholdPumpPhHigh = 0;

void PUMP_Init(uint32_t time)
{
	sttControlPump = STT_PUMP_SLEEP;
	nPumpPeriod = time;
	SET_PMIX((GPIO_PinState)SET_OFF);
	SET_PUMP_EC((GPIO_PinState)SET_OFF);
	SET_PUMP_PH((GPIO_PinState)SET_OFF);
}

bool PUMP_Timer1s(void)
{
	if (nPumpPeriod > 0)
		nPumpPeriod--;
	if (nTimeOnMotorMix > 0)
		nTimeOnMotorMix--;
	if (nTimeCheckEc > 0)
		nTimeCheckEc--;

	return true;
}

bool PUMP_Process(void)
{
	if (modeSetting != SETTING_ENABLE)
	{
		/* Turn on motor mix */
		if (nTimeOnMotorMix != 0)
		{
			SET_PMIX((GPIO_PinState)SET_ON);
		}
		else
		{
			SET_PMIX((GPIO_PinState)SET_OFF);
		}

		/* Turn on pump ph */
		if (nTimeOnPumpPeriPh != 0)
		{
			SET_PUMP_PH((GPIO_PinState)SET_ON);
		}
		else
		{
			SET_PUMP_PH((GPIO_PinState)SET_OFF);
		}

		/* Turn on pump ec */
		if (nTimeOnPumpPeriEc != 0)
		{
			SET_PUMP_EC((GPIO_PinState)SET_ON);
		}
		else
		{
			SET_PUMP_EC((GPIO_PinState)SET_OFF);
		}
	}
	
	return true;
}

bool PUMP_Timer1ms(void)
{
	if (nTimeOnPumpPeriEc > 0)
		nTimeOnPumpPeriEc--;
	if (nTimeOnPumpPeriPh > 0)
		nTimeOnPumpPeriPh--;
	
	return true;
}

bool PUMP_Control(void)
{
	/* check solenoid */
	switch (sttControlPump)
	{
	case STT_PUMP_SLEEP:
		if (nPumpPeriod == 0)
		{
			nTimeCheckEc = 10;
			sttControlPump = STT_CHECK_SENSOR;
			nPumpPeriod = tFamInfo.nTimeUpload; // start new period
		}
		break;
	case STT_CHECK_SENSOR:
		if (nTimeCheckEc == 0)
		{
			/* set threshold for control pump */
			nThresholdPumpEcLow = tFamInfo.nThrehold_EcLow - 20;
			nThresholdPumpPhHigh = tFamInfo.nThrehold_PHHigh + 150;
			
			// control pump Mix
			if ((tSensorIrriValue.dEcAvg*100 < (double)tFamInfo.nThrehold_Ec) && (tSensorIrriValue.dEcAvg*100 >= (double)nThresholdPumpEcLow))
			{
				nTimeOnMotorMix = tFamInfo.nPump_TimePumpMix;
				sttControlPump = STT_CTL_MOTOR_MIX;
			}
			else if ((tSensorIrriValue.dPhAvg*100 > (double)tFamInfo.nThrehold_PH) && (tSensorIrriValue.dPhAvg*100 <= (double)nThresholdPumpPhHigh))
			{
				nTimeOnMotorMix = tFamInfo.nPump_TimePumpMix;
				sttControlPump = STT_CTL_MOTOR_MIX;
			}
			else
			{
				sttControlPump = STT_UPDATE_SERVER;
			}
			
			/*
			if ((tSensorIrriValue.tEcCheck == CHECK_EC_OK) || (tSensorIrriValue.tPhCheck == CHECK_PH_OK))
			{
				if ((tSensorIrriValue.dEcAvg*100 < (double)tFamInfo.nThrehold_Ec) && (tSensorIrriValue.dEcAvg*100 > (double)nThresholdPumpEcLow))
				{
					nTimeOnMotorMix = tFamInfo.nPump_TimePumpMix;
					sttControlPump = STT_CTL_MOTOR_MIX;
				}
				else if ((tSensorIrriValue.dPhAvg*100 > (double)tFamInfo.nThrehold_PH) && (tSensorIrriValue.dPhAvg*100 < (double)nThresholdPumpPhHigh))
				{
					nTimeOnMotorMix = tFamInfo.nPump_TimePumpMix;
					sttControlPump = STT_CTL_MOTOR_MIX;
				}
				else
				{
					sttControlPump = STT_UPDATE_SERVER;
				}
			}
			else
			{
				sttControlPump = STT_UPDATE_SERVER;
			}
			*/
		}

		break;
	case STT_CTL_MOTOR_MIX:
		if (nTimeOnMotorMix == 0)
		{
			sttControlPump = STT_CTL_MOTOR_PERI;
		}
		break;
	case STT_CTL_MOTOR_PERI:
		/* control EC pump */
		switch (tSensorIrriValue.tEcCheck)
		{
			case CHECK_EC_OK:
				if (tSensorIrriValue.dEcAvg*100 < (double)(tFamInfo.nThrehold_Ec)){
					nTimeOnPumpPeriEc = tFamInfo.nPump_TimePeri * 500;
				}else{
					nTimeOnPumpPeriEc = 0;
				}
				break;
			case CHECK_EC_LOW:
				if (tSensorIrriValue.dEcAvg*100 > (double)(nThresholdPumpEcLow)){
					nTimeOnPumpPeriEc = tFamInfo.nPump_TimePeri * 1000;
				}else{
					nTimeOnPumpPeriEc = 0;
				}
				break;
			case CHECK_EC_HIGH:
			case CHECK_EC_DUAL:
				nTimeOnPumpPeriEc = 0; // stop pump
				break;
		}
//		if (tSensorIrriValue.tEcCheck == CHECK_EC_OK)
//		{
//			if (tSensorIrriValue.dEcAvg*100 < (double)(tFamInfo.nThrehold_Ec - 10))
//			{
//				nTimeOnPumpPeriEc = tFamInfo.nPump_TimePeri * 1000;
//			}
//			else if (tSensorIrriValue.dEcAvg*100 >= (double)(tFamInfo.nThrehold_Ec - 10) && (double)(tSensorIrriValue.dEcAvg*100 < tFamInfo.nThrehold_Ec))
//			{
//				nTimeOnPumpPeriEc = tFamInfo.nPump_TimePeri * 500;
//			}
//			else
//			{
//				nTimeOnPumpPeriEc = 0;
//			}
//		}

		/* control PH pump */
		switch (tSensorIrriValue.tPhCheck)
		{
			case CHECK_PH_OK:
				if (tSensorIrriValue.dPhAvg*100 > (double)(tFamInfo.nThrehold_PH)){
					nTimeOnPumpPeriPh = tFamInfo.nPump_TimePeri * 500;
				}else{
					nTimeOnPumpPeriPh = 0;
				}
				break;
			case CHECK_PH_HIGH:
				if (tSensorIrriValue.dPhAvg*100 < (double)(nThresholdPumpPhHigh)){
					nTimeOnPumpPeriPh = tFamInfo.nPump_TimePeri * 1000;
				}else{
					nTimeOnPumpPeriPh = 0;
				}
				break;
			case CHECK_PH_LOW:
			case CHECK_PH_DUAL:
				nTimeOnPumpPeriPh = 0;
				break;
		}
//		if (tSensorIrriValue.tPhCheck == CHECK_PH_OK)
//		{
//			if (tSensorIrriValue.dPhAvg*100 > (double)(tFamInfo.nThrehold_PH + 50))
//			{
//				nTimeOnPumpPeriPh = tFamInfo.nPump_TimePeri * 1000;
//			}
//			else if (tSensorIrriValue.dPhAvg*100 <= (double)(tFamInfo.nThrehold_PH + 50) && (double)(tSensorIrriValue.dPhAvg*100 > tFamInfo.nThrehold_PH))
//			{
//				nTimeOnPumpPeriPh = tFamInfo.nPump_TimePeri * 500;
//			}
//			else
//			{
//				nTimeOnPumpPeriPh = 0;
//			}
//		}

		sttControlPump = STT_UPDATE_SERVER;
		break;
	case STT_UPDATE_SERVER:
		//TODO: Ethernet Exclude
		//EthState = ETHERNET_CHECK;
		sttControlPump = STT_PUMP_SLEEP;
		break;
	}

	return true;
}
