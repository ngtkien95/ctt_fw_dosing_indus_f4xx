#ifndef __INCLUDE_H
#define __INCLUDE_H

#define _USE_MATH_DEFINES

#include "main.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "FLASH/FlashConfig.h"
#include "LCD4/lcd4.h"
#include "DS3231/ds3231.h"
#include "BUTTON/button.h"
#include "MODE/mode.h"
#include "SENSOR/sensor.h"
#include "SENSOR/hcsr04.h"
#include "SENSOR/cliSensor.h"
#include "SENSOR/yfb1.h"
#include <gpioMachine.h>
#include "PUMP/pump.h"
//#include "W5500/ethw5500.h"
#include "W5500/jsonProcess.h"
#include "config.h"

//#define TEST_MODE

#endif
