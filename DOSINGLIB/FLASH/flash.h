/*
 * flash.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __FLASH_H
#define __FLASH_H

#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <stdbool.h>

void FLASH_Read(uint32_t adr, int32_t *data);
void FLASH_ReadString(uint32_t adr, __IO uint32_t * data);
void FLASH_Unlock(void);
void FLASH_Lock(void);
void FLASH_Erase(uint32_t adr);
void FLASH_Write(uint32_t adr, uint16_t data);
void FLASH_WriteWord(uint32_t adr, uint32_t data);
void FLASH_WriteString(uint32_t adr, uint32_t * data);
void TEST_FLASH(void);
#endif
