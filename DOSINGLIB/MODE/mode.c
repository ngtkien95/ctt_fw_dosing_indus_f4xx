/*
 * mode.c
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#include "../include.h"

#define MAX_LCD_LEN 30
#define TIMERDEBOUNCEMAX 100 //Chong rung phim
#define TIMERCHECKLONGCLICK 1000
#define TIMERHANDLELONGCLICK 50
#define REFRESH 1500
extern _DS3231 tRtc;
extern _HCSR04 tDistance;
//extern _ETHW5500 ethCheck;
extern _SENSOR_IRRI tSensorIrriValue;
extern _SENSOR_CLI tSensorCliValue;
//extern Value_t m_value[100];
Value_t m_value[100];
_FAM_INFO tFamInfo;

int16_t nTimeRefreshLcd;
uint16_t nTimerCheckDebounce;
float fWaterTempStandard;
float fClimateTempStandard;
float fClimateHumiStandard;
uint16_t nClimateLightStandard;
uint16_t nTankLevelStandard1;
uint16_t nTankLevelStandard2;
uint16_t nTankLevelStandard3;
bool refreshLcd;
bool success;
bool bZigbeeState = true;
char ac_BuffLcd[30];

flagMode_t flagMode;
modeDisplay_t modeDisplay;
modeSetting_t modeSetting;
settingEthernet_t settingEthernet;
settingThreshold_t settingThreshold;
settingRtc_t settingRtc;
settingPump_t settingPump;
settingCalibEC_t settingCalibEC;
settingCalibWT_t settingCalibWT;
settingCalibPH_t settingCalibPH;
settingCalibTE_t settingCalibTE;
settingCalibHU_t settingCalibHU;
settingCalibLI_t settingCalibLI;
settingCalibFS_t settingCalibFS;
settingCalibTankLevel1_t settingCalibTankLevel1;
settingCalibTankLevel2_t settingCalibTankLevel2;
settingCalibTankLevel3_t settingCalibTankLevel3;
settingExIO_t settingExIO;
settingEnable_t settingEnable;
settingInfo_t settingInfo;

//Define
void Handle_ModeMainDisplay(void);
void Handle_ModeEthernetSetting(settingEthernet_t m_settingEthernet);
void Handle_ModeSettingThresHold(settingThreshold_t m_settingThreshold);
void Handle_ModeSettingPump(settingPump_t m_settingPump);
void Handle_ModeSettingCalibEC(settingCalibEC_t m_settingCalibEC);
void Handle_ModeSettingCalibWT(settingCalibWT_t m_settingCalibWT);
void Handle_ModeSettingCalibPH(settingCalibPH_t m_settingCalibPH);
void Handle_ModeSettingCalibTE(settingCalibTE_t m_settingCalibTE);
void Handle_ModeSettingCalibHU(settingCalibHU_t m_settingCalibHU);
void Handle_ModeSettingCalibLi(settingCalibLI_t m_settingCalibLI);
void Handle_ModeSettingCalibFS(settingCalibFS_t m_settingCalibFS);
void Handle_ModeSettingEXIO(settingExIO_t m_settingExIO);
void Handle_ModeSettingEnableIO(settingEnable_t m_settingEnable);
void Handle_ModeSettingRTC(settingRtc_t m_settingRtc);
//Declear
void MODE_Init(void)
{
	flagMode = MODE_DISPLAY;
	modeDisplay = DISPLAY_EC_PH_WT;
	settingEthernet = SETTING_ETHERNET_NONE;
	settingRtc = SETTING_RTC_NONE;
	settingThreshold = SETTING_THRESHOLD_NONE;
	settingPump = SETTING_PUMP_NONE;
	settingCalibEC = SETTING_CALIB_EC_NONE;
	settingCalibWT = SETTING_CALIB_WT_NONE;
	settingExIO = SETTING_EXIO_NONE;
	settingEnable = SETTING_ENABLE_NONE;
	settingInfo = SETTING_INFO_NONE;
	LCD4_Clear();
}

void MODE_Disp_Success(void)
{
	flagMode = MODE_CONFIG_SUCCESS;
	success = true;
}
void Handle_ModeMainDisplay(void){
	switch (modeDisplay)
	{
	case DISPLAY_EC_PH_WT:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}

		// RTC and farmName
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%s  ", tFamInfo.cFarmName);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%02d:%02d:%02d", tRtc.Hour, tRtc.Min, tRtc.Sec);
		LCD4_WriteCursor(0, 12, (int8_t *)ac_BuffLcd);


		snprintf(ac_BuffLcd,MAX_LCD_LEN, "PH: %01.02f  %01.02f", tSensorIrriValue.dPh1, tSensorIrriValue.dPh2);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "EC: %01.02f  %01.02f", tSensorIrriValue.dEc1, tSensorIrriValue.dEc2);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "WT: %02.02f %02.02f", tSensorIrriValue.dWaterTemp1, tSensorIrriValue.dWaterTemp2);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		if (nTimeRefreshLcd > REFRESH)
		{



			switch (tSensorIrriValue.tEcCheck)
			{
			case CHECK_EC_HIGH:
				LCD4_WriteCursor(2, 16, (int8_t *)"HIGH");
				break;
			case CHECK_EC_LOW:
				LCD4_WriteCursor(2, 16, (int8_t *)"LOW");
				break;
			case CHECK_EC_DUAL:
				LCD4_WriteCursor(2, 16, (int8_t *)"DUAL");
				break;
			default:
				break;
			}
			switch (tSensorIrriValue.tWtCheck)
			{
			case CHECK_WT_HIGH:
				LCD4_WriteCursor(3, 16, (int8_t *)"HIGH");
				break;
			case CHECK_WT_LOW:
				LCD4_WriteCursor(3, 16, (int8_t *)"LOW");
				break;
			case CHECK_WT_DUAL:
				LCD4_WriteCursor(3, 16, (int8_t *)"DUAL");
				break;
			default:
				break;
			}
			switch (tSensorIrriValue.tPhCheck)
			{
			case CHECK_PH_HIGH:
				LCD4_WriteCursor(1, 16, (int8_t *)"HIGH");
				break;
			case CHECK_PH_LOW:
				LCD4_WriteCursor(1, 16, (int8_t *)"LOW");
				break;
			case CHECK_PH_DUAL:
				LCD4_WriteCursor(1, 16, (int8_t *)"DUAL");
				break;
			default:
				break;
			}
		}
		else
		{
			//				LCD4_WriteCursor(1, 16, (int8_t *)"    ");
			//				LCD4_WriteCursor(2, 16, (int8_t *)"    ");
			//				LCD4_WriteCursor(3, 16, (int8_t *)"    ");
		}
		break;
	case DISPLAY_TE_HU_LI:
	{
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}

		// RTC and farmName
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%s  ", tFamInfo.cFarmName);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%02d:%02d:%02d", tRtc.Hour, tRtc.Min, tRtc.Sec);
		LCD4_WriteCursor(0, 12, (int8_t *)ac_BuffLcd);

		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Te: %02.01f %02.01f", tSensorCliValue.fCliTemp1, tSensorCliValue.fCliTemp2);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Hu: %02.02f %02.02f", tSensorCliValue.fCliHumi1, tSensorCliValue.fCliHumi2);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Li: %05d %05d", tSensorCliValue.nCliLight1, tSensorCliValue.nCliLight2);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);

		if (nTimeRefreshLcd > REFRESH)
		{
			switch (tSensorCliValue.tTempCheck)
			{
			case CHECK_TEMP_HIGH:
				LCD4_WriteCursor(1, 16, (int8_t *)"HIGH");
				break;
			case CHECK_TEMP_LOW:
				LCD4_WriteCursor(1, 16, (int8_t *)"LOW");
				break;
			case CHECK_TEMP_DUAL:
				LCD4_WriteCursor(1, 16, (int8_t *)"DUAL");
				break;
			default:
				break;
			}
			switch (tSensorCliValue.tHumiCheck)
			{
			case CHECK_HUMI_HIGH:
				LCD4_WriteCursor(2, 16, (int8_t *)"HIGH");
				break;
			case CHECK_HUMI_LOW:
				LCD4_WriteCursor(2, 16, (int8_t *)"LOW");
				break;
			case CHECK_HUMI_DUAL:
				LCD4_WriteCursor(2, 16, (int8_t *)"DUAL");
				break;
			default:
				break;
			}
			switch (tSensorCliValue.tLightCheck)
			{
			case CHECK_LIGHT_HIGH:
				LCD4_WriteCursor(3, 16, (int8_t *)"HIGH");
				break;
			case CHECK_LIGHT_LOW:
				LCD4_WriteCursor(3, 16, (int8_t *)"LOW");
				break;
			case CHECK_LIGHT_DUAL:
				LCD4_WriteCursor(3, 16, (int8_t *)"DUAL");
				break;
			default:
				break;
			}
		}
		else
		{
			//				LCD4_WriteCursor(1, 16, (int8_t *)"    ");
			//				LCD4_WriteCursor(2, 16, (int8_t *)"    ");
			//				LCD4_WriteCursor(3, 16, (int8_t *)"    ");
		}
	}
		break;
	case DISPLAY_FS_US:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}

		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Flow : xxx mL/m");
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Tank1: %3d %3d", tDistance.dLitUs1, tDistance.dLitUs2);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Tank2: %3d %3d", tDistance.dLitUs3, tDistance.dLitUs4);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Tank3: %3d %3d", tDistance.dLitUs5, tDistance.dLitUs6);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);

		if (nTimeRefreshLcd > REFRESH)
		{
			switch (tDistance.tTank1Check)
			{
			case CHECK_TANK1_HIGH:
				LCD4_WriteCursor(1, 16, (int8_t *)"HIGH");
				break;
			case CHECK_TANK1_LOW:
				LCD4_WriteCursor(1, 16, (int8_t *)"LOW");
				break;
			case CHECK_TANK1_DUAL:
				LCD4_WriteCursor(1, 16, (int8_t *)"DUAL");
				break;
			default:
				break;
			}
			switch (tDistance.tTank2Check)
			{
			case CHECK_TANK2_HIGH:
				LCD4_WriteCursor(2, 16, (int8_t *)"HIGH");
				break;
			case CHECK_TANK2_LOW:
				LCD4_WriteCursor(2, 16, (int8_t *)"LOW");
				break;
			case CHECK_TANK2_DUAL:
				LCD4_WriteCursor(2, 16, (int8_t *)"DUAL");
				break;
			default:
				break;
			}
			switch (tDistance.tTank3Check)
			{
			case CHECK_TANK3_HIGH:
				LCD4_WriteCursor(3, 16, (int8_t *)"HIGH");
				break;
			case CHECK_TANK3_LOW:
				LCD4_WriteCursor(3, 16, (int8_t *)"LOW");
				break;
			case CHECK_TANK3_DUAL:
				LCD4_WriteCursor(3, 16, (int8_t *)"DUAL");
				break;
			default:
				break;
			}
		}
		else
		{
			LCD4_WriteCursor(1, 16, (int8_t *)"    ");
			LCD4_WriteCursor(2, 16, (int8_t *)"    ");
			LCD4_WriteCursor(3, 16, (int8_t *)"    ");
		}
		break;
	}
}
void Handle_ModeEthernetSetting(settingEthernet_t m_settingEthernet){
	switch (m_settingEthernet)
	{
	case SETTING_ETHERNET_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}

		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "    %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"1. Internet");
		LCD4_WriteCursor(2, 0, (int8_t *)"2. Threshold");
		LCD4_WriteCursor(3, 0, (int8_t *)"3. Pump");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	default:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "CHUA HOAT DONG");
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);

		//				snprintf(ac_BuffLcd,MAX_LCD_LEN, "IP: %15s ", ethCheck.nIpAddress);
		//				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);

		//				if (ethCheck.bState)
		//					snprintf(ac_BuffLcd,MAX_LCD_LEN, "<<  Connected   >>");
		//				else
		//					snprintf(ac_BuffLcd,MAX_LCD_LEN, "<< Disconnected >>");
		//				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);

		//				if (bZigbeeState == true)
		//					snprintf(ac_BuffLcd,MAX_LCD_LEN, "Zigbee State:   ON");
		//				else
		//					snprintf(ac_BuffLcd,MAX_LCD_LEN, "Zigbee State:  OFF");
		//				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		//
		//				snprintf(ac_BuffLcd,MAX_LCD_LEN, "T Update: %3d (s)", tFamInfo.nTimeUpload);
		//				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	}
	//			switch (settingEthernet)
	//			{
	//				case SETTING_ZIGBEE_STATE:
	//					LCD4_WriteCursor(2, 19, (int8_t *)"*");
	//					LCD4_WriteCursor(3, 19, (int8_t *)" ");
	//					break;
	//				case SETTING_ETHERNET_UPDATE:
	//					LCD4_WriteCursor(2, 19, (int8_t *)" ");
	//					LCD4_WriteCursor(3, 19, (int8_t *)"*");
	//					break;
	//				default:
	//					break;
	//			}
}
void Handle_ModeSettingThresHold(settingThreshold_t m_settingThreshold){
	switch (m_settingThreshold)
	{
	case SETTING_THRESHOLD_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "    %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"1. Internet");
		LCD4_WriteCursor(2, 0, (int8_t *)"2. Threshold");
		LCD4_WriteCursor(3, 0, (int8_t *)"3. Pump");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_THRESHOLD_EC:
	case SETTING_THRESHOLD_EC_LOW:
	case SETTING_THRESHOLD_EC_HIGH:
	case SETTING_THRESHOLD_EC_DUAL:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "EC:      %4.2f", (float)tFamInfo.nThrehold_Ec / 100);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "EC LOW:  %4.2f", (float)tFamInfo.nThrehold_EcLow / 100);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "EC HIGH: %4.2f", (float)tFamInfo.nThrehold_ECHigh / 100);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "EC DUAL: %4.2f", (float)tFamInfo.nThrehold_ECDual / 100);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	case SETTING_THRESHOLD_WATERTEMP:
	case SETTING_THRESHOLD_WATERTEMP_LOW:
	case SETTING_THRESHOLD_WATERTEMP_HIGH:
	case SETTING_THRESHOLD_WATERTEMP_DUAL:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "WT:      %2d", tFamInfo.nThrehold_WT / 10);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "WT LOW:  %2d", tFamInfo.nThrehold_WTLow / 10);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "WT HIGH: %2d", tFamInfo.nThrehold_WTHigh / 10);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "WT DUAL: %2d", tFamInfo.nThrehold_WTDual / 10);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	case SETTING_THRESHOLD_PH:
	case SETTING_THRESHOLD_PH_LOW:
	case SETTING_THRESHOLD_PH_HIGH:
	case SETTING_THRESHOLD_PH_DUAL:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "PH:       %4.2f", (float)tFamInfo.nThrehold_PH / 100);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "PH LOW:   %4.2f", (float)tFamInfo.nThrehold_PHLow / 100);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "PH HIGH:  %4.2f", (float)tFamInfo.nThrehold_PHHigh / 100);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "PH DUAL:  %4.2f", (float)tFamInfo.nThrehold_PHDual / 100);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	case SETTING_THRESHOLD_TE:
	case SETTING_THRESHOLD_TE_LOW:
	case SETTING_THRESHOLD_TE_HIGH:
	case SETTING_THRESHOLD_TE_DUAL:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "TE:      %2d", tFamInfo.nThrehold_Cli_Temp / 10);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "TE LOW:  %2d", tFamInfo.nThrehold_Cli_TempLow / 10);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "TE HIGH: %2d", tFamInfo.nThrehold_Cli_TempHigh / 10);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "TE DUAL: %2d", tFamInfo.nThrehold_Cli_TempDual / 10);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	case SETTING_THRESHOLD_HU:
	case SETTING_THRESHOLD_HU_LOW:
	case SETTING_THRESHOLD_HU_HIGH:
	case SETTING_THRESHOLD_HU_DUAL:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "HU:      %2d", tFamInfo.nThrehold_Cli_Humi / 10);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "HU LOW:  %2d", tFamInfo.nThrehold_Cli_HumiLow / 10);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "HU HIGH: %2d", tFamInfo.nThrehold_Cli_HumiHigh / 10);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "HU DUAL: %2d", tFamInfo.nThrehold_Cli_HumiDual / 10);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	case SETTING_THRESHOLD_LI:
	case SETTING_THRESHOLD_LI_LOW:
	case SETTING_THRESHOLD_LI_HIGH:
	case SETTING_THRESHOLD_LI_DUAL:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "LI:      %5d", tFamInfo.nThrehold_Cli_Light);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "LI LOW:  %5d", tFamInfo.nThrehold_Cli_LightLow);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "LI HIGH: %5d", tFamInfo.nThrehold_Cli_LightHigh);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "LI DUAL: %5d", tFamInfo.nThrehold_Cli_LightDual);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	case SETTING_THRESHOLD_FS:
	case SETTING_THRESHOLD_FS_LOW:
	case SETTING_THRESHOLD_FS_HIGH:
	case SETTING_THRESHOLD_FS_DUAL:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "FS:      %2d", tFamInfo.nThrehold_FlowRate);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "FS LOW:  %2d", tFamInfo.nThrehold_FlowRateLow);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "FS HIGH: %2d", tFamInfo.nThrehold_FlowRateHigh);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "FS DUAL: %2d", tFamInfo.nThrehold_FlowRateDual);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	case SETTING_THRESHOLD_TANKLEVEL:
	case SETTING_THRESHOLD_TANKLEVEL_LOW:
	case SETTING_THRESHOLD_TANKLEVEL_HIGH:
	case SETTING_THRESHOLD_TANKLEVEL_DUAL:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "LEVEL T:      %3d", tFamInfo.nThrehold_TankLevel / 10);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "LEVEL T LOW:  %2d", tFamInfo.nThrehold_TankLevelLow / 10);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "LEVEL T HIGH: %2d", tFamInfo.nThrehold_TankLevelHigh / 10);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "LEVEL T DUAL: %2d", tFamInfo.nThrehold_TankLevelDual / 10);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	}
	switch (settingThreshold)
	{
	case SETTING_THRESHOLD_EC:
	case SETTING_THRESHOLD_WATERTEMP:
	case SETTING_THRESHOLD_PH:
	case SETTING_THRESHOLD_TE:
	case SETTING_THRESHOLD_HU:
	case SETTING_THRESHOLD_LI:
	case SETTING_THRESHOLD_FS:
	case SETTING_THRESHOLD_TANKLEVEL:
		LCD4_WriteCursor(0, 19, (int8_t *)"*");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_THRESHOLD_EC_LOW:
	case SETTING_THRESHOLD_WATERTEMP_LOW:
	case SETTING_THRESHOLD_PH_LOW:
	case SETTING_THRESHOLD_TE_LOW:
	case SETTING_THRESHOLD_HU_LOW:
	case SETTING_THRESHOLD_LI_LOW:
	case SETTING_THRESHOLD_FS_LOW:
	case SETTING_THRESHOLD_TANKLEVEL_LOW:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_THRESHOLD_EC_HIGH:
	case SETTING_THRESHOLD_WATERTEMP_HIGH:
	case SETTING_THRESHOLD_PH_HIGH:
	case SETTING_THRESHOLD_TE_HIGH:
	case SETTING_THRESHOLD_HU_HIGH:
	case SETTING_THRESHOLD_LI_HIGH:
	case SETTING_THRESHOLD_FS_HIGH:
	case SETTING_THRESHOLD_TANKLEVEL_HIGH:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_THRESHOLD_EC_DUAL:
	case SETTING_THRESHOLD_WATERTEMP_DUAL:
	case SETTING_THRESHOLD_PH_DUAL:
	case SETTING_THRESHOLD_TE_DUAL:
	case SETTING_THRESHOLD_HU_DUAL:
	case SETTING_THRESHOLD_LI_DUAL:
	case SETTING_THRESHOLD_FS_DUAL:
	case SETTING_THRESHOLD_TANKLEVEL_DUAL:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	}
}
void Handle_ModeSettingPump(settingPump_t m_settingPump) {
	switch (m_settingPump) {
	case SETTING_PUMP_NONE:
		if (refreshLcd == true) {
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t*) "Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "    %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t*) ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t*) "1. Internet");
		LCD4_WriteCursor(2, 0, (int8_t*) "2. Threshold");
		LCD4_WriteCursor(3, 0, (int8_t*) "3. Pump");
		LCD4_WriteCursor(1, 19, (int8_t*) " ");
		LCD4_WriteCursor(2, 19, (int8_t*) " ");
		LCD4_WriteCursor(3, 19, (int8_t*) "*");
		break;
	default:
		if (refreshLcd == true) {
			LCD4_Clear();
			refreshLcd = false;
		}

		LCD4_WriteCursor(0, 0, (int8_t*) ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "T Pump Mix  :%3d(s)", tFamInfo.nPump_TimePumpMix);
		LCD4_WriteCursor(1, 0, (int8_t*) ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "T Nozzle    :%3d(s)", tFamInfo.nPump_TimeNozzleOn);
		LCD4_WriteCursor(2, 0, (int8_t*) ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "T Noz Period:%3d(s)",
				tFamInfo.nPump_TimeNozzleOff);
		LCD4_WriteCursor(3, 0, (int8_t*) ac_BuffLcd);
		break;
	}
	switch (settingPump) {
	case SETTING_TIME_PUMP_PERI:
		LCD4_WriteCursor(0, 19, (int8_t*) "*");
		LCD4_WriteCursor(1, 19, (int8_t*) " ");
		LCD4_WriteCursor(2, 19, (int8_t*) " ");
		LCD4_WriteCursor(3, 19, (int8_t*) " ");
		break;
	case SETTING_TIME_PUMP_MIX:
		LCD4_WriteCursor(0, 19, (int8_t*) " ");
		LCD4_WriteCursor(1, 19, (int8_t*) "*");
		LCD4_WriteCursor(2, 19, (int8_t*) " ");
		LCD4_WriteCursor(3, 19, (int8_t*) " ");
		break;
	case SETTING_TIME_PUMP_NOZZLE:
		LCD4_WriteCursor(0, 19, (int8_t*) " ");
		LCD4_WriteCursor(1, 19, (int8_t*) " ");
		LCD4_WriteCursor(2, 19, (int8_t*) "*");
		LCD4_WriteCursor(3, 19, (int8_t*) " ");
		break;
	case SETTING_TIME_PUMP_NOZZLE_WAIT:
		LCD4_WriteCursor(0, 19, (int8_t*) " ");
		LCD4_WriteCursor(1, 19, (int8_t*) " ");
		LCD4_WriteCursor(2, 19, (int8_t*) " ");
		LCD4_WriteCursor(3, 19, (int8_t*) "*");
		break;
	default:
		break;
	}
}
void Handle_ModeSettingCalibEC(settingCalibEC_t m_settingCalibEC){
	switch (m_settingCalibEC)
	{
	case SETTING_CALIB_EC_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "    %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"4. Calib EC");
		LCD4_WriteCursor(2, 0, (int8_t *)"5. Calib wTemp");
		LCD4_WriteCursor(3, 0, (int8_t *)"6. Calib PH");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_EC_STANDARD:
	case SETTING_CLICK_CALIB_EC:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
		LCD4_WriteCursor(1, 0, (int8_t *)"the EC solution");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "EC sol: %4d mS/cm", tFamInfo.nStandard_Ec);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
		break;
	case SETTING_CALIB_EC1_A:
	case SETTING_CALIB_EC1_B:
	case SETTING_CALIB_EC2_A:
	case SETTING_CALIB_EC2_B:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Ec1: %2.2f-> A:%4d", tSensorIrriValue.dEc1, tFamInfo.nCalib_Ec1A);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%4.0f mV   > B:%4d", tSensorIrriValue.dEc1mv, tFamInfo.nCalib_Ec1B);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Ec2: %2.2f-> A:%4d", tSensorIrriValue.dEc2, tFamInfo.nCalib_Ec2A);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%4.0f mV   > B:%4d", tSensorIrriValue.dEc2mv, tFamInfo.nCalib_Ec2B);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	default:
		break;
	}
	switch (settingCalibEC)
	{
	case SETTING_EC_STANDARD:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CLICK_CALIB_EC:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	case SETTING_CALIB_EC1_A:
		LCD4_WriteCursor(0, 19, (int8_t *)"*");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CALIB_EC1_B:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CALIB_EC2_A:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CALIB_EC2_B:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	default:
		break;
	}
}
void Handle_ModeSettingCalibWT(settingCalibWT_t m_settingCalibWT){
	switch (m_settingCalibWT)
	{
	case SETTING_CALIB_WT_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "    %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"4. Calib EC");
		LCD4_WriteCursor(2, 0, (int8_t *)"5. Calib wTemp");
		LCD4_WriteCursor(3, 0, (int8_t *)"6. Calib PH");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_WT_STANDARD:
	case SETTING_CLICK_CALIB_WT:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
		LCD4_WriteCursor(1, 0, (int8_t *)"the Water Temp Sol");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "WT Stand: %2.2f ", fWaterTempStandard);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		LCD4_WriteCommand(1, 223);
		LCD4_WriteCommand(1, 67);
		LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
		break;
	default:
		break;
	}
	switch (settingCalibWT)
	{
	case SETTING_WT_STANDARD:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CLICK_CALIB_WT:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	default:
		break;
	}
}
void Handle_ModeSettingCalibPH(settingCalibPH_t m_settingCalibPH){
	switch (m_settingCalibPH)
	{
	case SETTING_CALIB_PH_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "    %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"4. Calib EC");
		LCD4_WriteCursor(2, 0, (int8_t *)"5. Calib wTemp");
		LCD4_WriteCursor(3, 0, (int8_t *)"6. Calib PH");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	case SETTING_PH_STANDARD:
	case SETTING_CLICK_CALIB_PH:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
		LCD4_WriteCursor(1, 0, (int8_t *)"the PH solution");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "PH solution: %4.2f  ", (double)tFamInfo.nStandard_Ph / 100);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
		break;
	case SETTING_CALIB_PH1_A:
	case SETTING_CALIB_PH1_B:
	case SETTING_CALIB_PH2_A:
	case SETTING_CALIB_PH2_B:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Ph1: %3.2f-> A:%4d", tSensorIrriValue.dPh1, tFamInfo.nCalib_Ph1A);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%4.0f mV   > B:%4d", tSensorIrriValue.dPh1mv, tFamInfo.nCalib_Ph1B);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Ph2: %3.2f-> A:%4d", tSensorIrriValue.dPh2, tFamInfo.nCalib_Ph2A);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%4.0f mV   > B:%4d", tSensorIrriValue.dPh2mv, tFamInfo.nCalib_Ph2B);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	default:
		break;
	}
	switch (settingCalibPH)
	{
	case SETTING_PH_STANDARD:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CLICK_CALIB_PH:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	case SETTING_CALIB_PH1_A:
		LCD4_WriteCursor(0, 19, (int8_t *)"*");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CALIB_PH1_B:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CALIB_PH2_A:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CALIB_PH2_B:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	default:
		break;
	}
}
void Handle_ModeSettingCalibTE(settingCalibTE_t m_settingCalibTE){
	switch (m_settingCalibTE)
	{
	case SETTING_CALIB_TE_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "    %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"7. Calib Cli Temp");
		LCD4_WriteCursor(2, 0, (int8_t *)"8. Calib Cli Humi");
		LCD4_WriteCursor(3, 0, (int8_t *)"9. Calib Cli Light");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CALIB_TE_STAND:
		//			case SETTING_CALIB_TE_CLICK:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Temperature Standard");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%2.2f ", fClimateTempStandard);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		LCD4_WriteCommand(1, 223);
		LCD4_WriteCommand(1, 67);
		//				LCD4_WriteCursor(2, 0, (int8_t *)"<< CALIBRATION >>");
		break;
	default:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		break;
	}
}
void Handle_ModeSettingCalibHU(settingCalibHU_t m_settingCalibHU){
	switch (m_settingCalibHU)
	{
	case SETTING_CALIB_HU_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "    %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"7. Calib Cli Temp");
		LCD4_WriteCursor(2, 0, (int8_t *)"8. Calib Cli Humi");
		LCD4_WriteCursor(3, 0, (int8_t *)"9. Calib Cli Light");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_CALIB_HU_STAND:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Humidity Standard");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%2.2f ", fClimateHumiStandard);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		LCD4_WriteCommand(1, 37);
		break;
	default:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		break;
	}
}
void Handle_ModeSettingCalibLi(settingCalibLI_t m_settingCalibLI){
	switch (m_settingCalibLI)
	{
	case SETTING_CALIB_LI_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "    %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"7. Calib Cli Temp");
		LCD4_WriteCursor(2, 0, (int8_t *)"8. Calib Cli Humi");
		LCD4_WriteCursor(3, 0, (int8_t *)"9. Calib Cli Light");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	case SETTING_CALIB_LI_STAND:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Light Standard");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "%d Lux", nClimateLightStandard);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		break;
	default:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		break;
	}
}
void Handle_ModeSettingCalibFS(settingCalibFS_t m_settingCalibFS){
	switch (m_settingCalibFS)
	{
	case SETTING_CALIB_FS_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "   %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"10. Calib Flow");
		LCD4_WriteCursor(2, 0, (int8_t *)"11. Calib US Tank1");
		LCD4_WriteCursor(3, 0, (int8_t *)"12. Calib US Tank2");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	default:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		break;
	}
}
void Handle_ModeSettingEXIO(settingExIO_t m_settingExIO){
	switch (m_settingExIO)
	{
	case SETTING_EXIO_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "   %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"13. Calib US Tank3");
		LCD4_WriteCursor(2, 0, (int8_t *)"14. EX IO");
		LCD4_WriteCursor(3, 0, (int8_t *)"15. Clock");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	default:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "T Nozzle On: %02d(h)", tFamInfo.nTime_NozzleOn);
		LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "T Nozzle Off:%02d(h)", tFamInfo.nTime_NozzleOff);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "T CooPad On: %02d(h)", tFamInfo.nTime_CoopadOn);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "T CooPad Off:%02d(h)", tFamInfo.nTime_CoopadOff);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	}
	switch (settingExIO)
	{
	case SETTING_EXIO_NOZZLE_TIME_ON:
		LCD4_WriteCursor(0, 19, (int8_t *)"*");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_EXIO_NOZZLE_TIME_OFF:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_EXIO_COOPAD_TIME_ON:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_EXIO_COOPAD_TIME_OFF:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	default:
		break;
	}
}
void Handle_ModeSettingEnableIO(settingEnable_t m_settingEnable){
	switch (m_settingEnable)
	{
	case SETTING_ENABLE_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "   %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"16. Devices Enable");
		LCD4_WriteCursor(2, 0, (int8_t *)"17. Default");
		LCD4_WriteCursor(3, 0, (int8_t *)"18. Info");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_ENABLE_EC:
	case SETTING_ENABLE_PH:
	case SETTING_ENABLE_O1:
	case SETTING_ENABLE_O2:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		if ((OP_EC_GPIO_Port->ODR & OP_EC_Pin) != 0x00u)
			LCD4_WriteCursor(0, 0, (int8_t *)"Pump EC        ON ");
		else
			LCD4_WriteCursor(0, 0, (int8_t *)"Pump EC        OFF");

		if ((OP_PH_GPIO_Port->ODR & OP_PH_Pin) != 0x00u)
			LCD4_WriteCursor(1, 0, (int8_t *)"Pump PH        ON ");
		else
			LCD4_WriteCursor(1, 0, (int8_t *)"Pump PH        OFF");

		if ((OP_O1_GPIO_Port->ODR & OP_O1_Pin) != 0x00u)
			LCD4_WriteCursor(2, 0, (int8_t *)"Nozzle         ON ");
		else
			LCD4_WriteCursor(2, 0, (int8_t *)"Nozzle         OFF");

		if ((OP_O2_GPIO_Port->ODR & OP_O2_Pin) != 0x00u)
			LCD4_WriteCursor(3, 0, (int8_t *)"Chiller        ON ");
		else
			LCD4_WriteCursor(3, 0, (int8_t *)"Chiller        OFF");
		break;
	case SETTING_ENABLE_O3:
	case SETTING_ENABLE_O4:
	case SETTING_ENABLE_O5:
	case SETTING_ENABLE_O6:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		if ((OP_O3_GPIO_Port->ODR & OP_O3_Pin) != 0x00u)
			LCD4_WriteCursor(0, 0, (int8_t *)"Cooling Pad    ON ");
		else
			LCD4_WriteCursor(0, 0, (int8_t *)"Cooling Pad    OFF");

		if ((OP_O4_GPIO_Port->ODR & OP_O4_Pin) != 0x00u)
			LCD4_WriteCursor(1, 0, (int8_t *)"Motor Mix      ON ");
		else
			LCD4_WriteCursor(1, 0, (int8_t *)"Motor Mix      OFF");

		if ((OP_O5_GPIO_Port->ODR & OP_O5_Pin) != 0x00u)
			LCD4_WriteCursor(2, 0, (int8_t *)"O5             ON ");
		else
			LCD4_WriteCursor(2, 0, (int8_t *)"O5             OFF");

		if ((OP_O6_GPIO_Port->ODR & OP_O6_Pin) != 0x00u)
			LCD4_WriteCursor(3, 0, (int8_t *)"O6             ON ");
		else
			LCD4_WriteCursor(3, 0, (int8_t *)"O6             OFF");
		break;
	case SETTING_ENABLE_O7:
	case SETTING_ENABLE_O8:
	case SETTING_ENABLE_O9:
	case SETTING_ENABLE_O10:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		if ((OP_O7_GPIO_Port->ODR & OP_O7_Pin) != 0x00u)
			LCD4_WriteCursor(0, 0, (int8_t *)"O7             ON ");
		else
			LCD4_WriteCursor(0, 0, (int8_t *)"O7             OFF");

		if ((OP_O8_GPIO_Port->ODR & OP_O8_Pin) != 0x00u)
			LCD4_WriteCursor(1, 0, (int8_t *)"O8             ON ");
		else
			LCD4_WriteCursor(1, 0, (int8_t *)"O8             OFF");

		if ((OP_O9_GPIO_Port->ODR & OP_O9_Pin) != 0x00u)
			LCD4_WriteCursor(2, 0, (int8_t *)"O9             ON ");
		else
			LCD4_WriteCursor(2, 0, (int8_t *)"O9             OFF");

		if ((OP_O10_GPIO_Port->ODR & OP_O10_Pin) != 0x00u)
			LCD4_WriteCursor(3, 0, (int8_t *)"O10            ON ");
		else
			LCD4_WriteCursor(3, 0, (int8_t *)"O10            OFF");
		break;
	default:
		break;
	}
	switch (settingEnable)
	{
	case SETTING_ENABLE_EC:
	case SETTING_ENABLE_O3:
	case SETTING_ENABLE_O7:
		LCD4_WriteCursor(0, 19, (int8_t *)"*");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_ENABLE_PH:
	case SETTING_ENABLE_O4:
	case SETTING_ENABLE_O8:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_ENABLE_O1:
	case SETTING_ENABLE_O5:
	case SETTING_ENABLE_O9:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case SETTING_ENABLE_O2:
	case SETTING_ENABLE_O6:
	case SETTING_ENABLE_O10:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	default:
		break;
	}
}
void Handle_ModeSettingRTC(settingRtc_t m_settingRtc){
	switch (m_settingRtc)
	{
	case SETTING_RTC_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "   %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"13. Calib US Tank3");
		LCD4_WriteCursor(2, 0, (int8_t *)"14. EX IO");
		LCD4_WriteCursor(3, 0, (int8_t *)"15. Clock");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	default:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting Clock");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Hour:    %02d", tRtc.Hour);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "Minute:  %02d", tRtc.Min);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		break;
	}
	switch (settingRtc)
	{
	case SETTING_RTC_HOUR:
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		break;
	case SETTING_RTC_MINUTE:
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		break;
	default:
		break;
	}
}
void Handle_ModeSettingInfo(settingInfo_t m_settingInfo){
	switch (m_settingInfo)
	{
	case SETTING_INFO_NONE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}
		LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, "   %01d", modeSetting);
		LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(1, 0, (int8_t *)"16. Devices Enable");
		LCD4_WriteCursor(2, 0, (int8_t *)"17. Default");
		LCD4_WriteCursor(3, 0, (int8_t *)"18. Info");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	case SETTING_INFO_FWVERSION:
	case SETTING_INFO_FARMCODE:
		if (refreshLcd == true)
		{
			LCD4_Clear();
			refreshLcd = false;
		}

		LCD4_WriteCursor(0, 0, (int8_t *)"FW Version");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, " %s", tFamInfo.cFwVersion);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		LCD4_WriteCursor(2, 0, (int8_t *)"Farm Code");
		snprintf(ac_BuffLcd,MAX_LCD_LEN, " %s", tFamInfo.cFarmCode);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	}
}
bool MODE_Lcd(void)
{
	//	char *ac_BuffLcd;
	//	ac_BuffLcd = (char *)malloc(20 * sizeof(char));

	switch (flagMode)
	{
	case MODE_DISPLAY:

		Handle_ModeMainDisplay();
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
//TODO: Ethernet Exclude
		case SETTING_INTERNET:
			//
			Handle_ModeEthernetSetting(settingEthernet);
			break;
		case SETTING_THRESHOLD:
			//
			Handle_ModeSettingThresHold(settingThreshold);
			break;
		case SETTING_PUMP:
			//Setting Pump
			Handle_ModeSettingPump(settingPump);
			break;
		case SETTING_CALIB_EC:
			Handle_ModeSettingCalibEC(settingCalibEC);
			break;
		case SETTING_CALIB_WT:
			Handle_ModeSettingCalibWT(settingCalibWT);
			break;
		case SETTING_CALIB_PH:
			Handle_ModeSettingCalibPH(settingCalibPH);
			break;
		case SETTING_CALIB_TE:
			Handle_ModeSettingCalibTE(settingCalibTE);
			//			switch (settingCalibTE)
			//			{
			//			case SETTING_CALIB_TE_STAND:
			//				LCD4_WriteCursor(1, 19, (int8_t *)"*");
			//				LCD4_WriteCursor(2, 19, (int8_t *)" ");
			//				break;
			//			case SETTING_CALIB_TE_CLICK:
			//				LCD4_WriteCursor(1, 19, (int8_t *)" ");
			//				LCD4_WriteCursor(2, 19, (int8_t *)"*");
			//				break;
			//			default:
			//				break;
			//			}
			break;
		case SETTING_CALIB_HU:
			Handle_ModeSettingCalibHU(settingCalibHU);
			break;
		case SETTING_CALIB_LI:
			Handle_ModeSettingCalibLi(settingCalibLI);
			break;
		case SETTING_CALIB_FS:
			Handle_ModeSettingCalibFS(settingCalibFS);
			break;
		case SETTING_CALIB_TANK_LEVEL_1:
			switch (settingCalibTankLevel1)
			{
			case SETTING_CALIB_TANK_LEVEL_1_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				snprintf(ac_BuffLcd,MAX_LCD_LEN, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"10. Calib Flow");
				LCD4_WriteCursor(2, 0, (int8_t *)"11. Calib US Tank1");
				LCD4_WriteCursor(3, 0, (int8_t *)"12. Calib US Tank2");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)"*");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_TANK_LEVEL_1_STAND:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Tank 1 Level");
				snprintf(ac_BuffLcd,MAX_LCD_LEN, "%d Liter", nTankLevelStandard1);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_2:
			switch (settingCalibTankLevel2)
			{
			case SETTING_CALIB_TANK_LEVEL_2_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				snprintf(ac_BuffLcd,MAX_LCD_LEN, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"10. Calib Flow");
				LCD4_WriteCursor(2, 0, (int8_t *)"11. Calib US Tank1");
				LCD4_WriteCursor(3, 0, (int8_t *)"12. Calib US Tank2");
				LCD4_WriteCursor(1, 19, (int8_t *)" ");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)"*");
				break;
			case SETTING_CALIB_TANK_LEVEL_2_STAND:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Tank 2 Level");
				snprintf(ac_BuffLcd,MAX_LCD_LEN, "%d Liter", nTankLevelStandard2);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_3:
			switch (settingCalibTankLevel3)
			{
			case SETTING_CALIB_TANK_LEVEL_3_NONE:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
				snprintf(ac_BuffLcd,MAX_LCD_LEN, "   %01d", modeSetting);
				LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(1, 0, (int8_t *)"13. Calib US Tank3");
				LCD4_WriteCursor(2, 0, (int8_t *)"14. EX IO");
				LCD4_WriteCursor(3, 0, (int8_t *)"15. Clock");
				LCD4_WriteCursor(1, 19, (int8_t *)"*");
				LCD4_WriteCursor(2, 19, (int8_t *)" ");
				LCD4_WriteCursor(3, 19, (int8_t *)" ");
				break;
			case SETTING_CALIB_TANK_LEVEL_3_STAND:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				LCD4_WriteCursor(0, 0, (int8_t *)"Tank 3 Level");
				snprintf(ac_BuffLcd,MAX_LCD_LEN, "%d Liter", nTankLevelStandard3);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				if (refreshLcd == true)
				{
					LCD4_Clear();
					refreshLcd = false;
				}
				break;
			}
			break;
		case SETTING_EXIO:
			Handle_ModeSettingEXIO(settingExIO);
			break;
		case SETTING_RTC:
			Handle_ModeSettingRTC(settingRtc);
			break;
		case SETTING_ENABLE:
			Handle_ModeSettingEnableIO(settingEnable);
			break;
		case SETTING_DEFAULT:
			if (refreshLcd == true)
			{
				LCD4_Clear();
				refreshLcd = false;
			}
			LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
			snprintf(ac_BuffLcd,MAX_LCD_LEN, "   %01d", modeSetting);
			LCD4_WriteCursor(0, 15, (int8_t *)ac_BuffLcd);
			LCD4_WriteCursor(1, 0, (int8_t *)"16. Devices Enable");
			LCD4_WriteCursor(2, 0, (int8_t *)"17. Default");
			LCD4_WriteCursor(3, 0, (int8_t *)"18. Info");
			LCD4_WriteCursor(1, 19, (int8_t *)" ");
			LCD4_WriteCursor(2, 19, (int8_t *)"*");
			LCD4_WriteCursor(3, 19, (int8_t *)" ");
			break;
		case SETTING_INFO:
			Handle_ModeSettingInfo(settingInfo);
			break;
		default:
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		LCD4_Clear();
		HAL_Delay(1);
		if (success == true)
			LCD4_WriteCursor(2, 7, (int8_t *)"Saved!");
		else
			LCD4_WriteCursor(2, 7, (int8_t *)"Failed!");
		HAL_Delay(1000);

		LCD4_Clear();
		HAL_Delay(1);

		flagMode = MODE_SETTING;
		settingEthernet = SETTING_ETHERNET_NONE;
		settingThreshold = SETTING_THRESHOLD_NONE;
		settingPump = SETTING_PUMP_NONE;
		settingCalibEC = SETTING_CALIB_EC_NONE;
		settingCalibWT = SETTING_CALIB_WT_NONE;
		settingCalibPH = SETTING_CALIB_PH_NONE;
		settingCalibTE = SETTING_CALIB_TE_NONE;
		settingCalibHU = SETTING_CALIB_HU_NONE;
		settingCalibLI = SETTING_CALIB_LI_NONE;
		settingCalibFS = SETTING_CALIB_FS_NONE;
		settingCalibTankLevel1 = SETTING_CALIB_TANK_LEVEL_1_NONE;
		settingCalibTankLevel2 = SETTING_CALIB_TANK_LEVEL_2_NONE;
		settingCalibTankLevel3 = SETTING_CALIB_TANK_LEVEL_3_NONE;
		settingExIO = SETTING_EXIO_NONE;
		settingEnable = SETTING_ENABLE_NONE;
		settingInfo = SETTING_INFO_NONE;
		break;
	}

	//	free(ac_BuffLcd);

	return true;
}

bool MODE_Timer(void)
{
	nTimeRefreshLcd++;
	if (nTimeRefreshLcd > REFRESH + 30)
		nTimeRefreshLcd = 0;

	switch (ReadButtonMode())
	{
	case BUTTON_MODE_CLICK:
		HandleButtonMode();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_MODE_LONGCLICK:
		break;
	default:
		break;
	}
	switch (ReadButtonSet())
	{
	case BUTTON_SET_CLICK:
		HandleButtonSet();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_SET_LONGCLICK:
		nTimerCheckDebounce++;
		break;
	default:
		break;
	}
	switch (ReadButtonUp())
	{
	case BUTTON_UP_CLICK:
		HandleButtonUp();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_UP_LONGCLICK:
		if (nTimerCheckDebounce > TIMERHANDLELONGCLICK)
		{
			HandleButtonUp();
			nTimerCheckDebounce = 0;
		}
		else
		{
			nTimerCheckDebounce++;
		}
		break;
	default:
		break;
	}
	switch (ReadButtonDown())
	{
	case BUTTON_DOWN_CLICK:
		HandleButtonDown();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_DOWN_LONGCLICK:
		if (nTimerCheckDebounce > TIMERHANDLELONGCLICK)
		{
			HandleButtonDown();
			nTimerCheckDebounce = 0;
		}
		else
		{
			nTimerCheckDebounce++;
		}
		break;
	default:
		break;
	}

	return true;
}



/* load params */
bool MODE_ParamsServerLoad(void)
{
	int index = 0;

	//HAL_FLASH_Unlock();

	FLASH_ReadString(EEPROM_START_ADDRESS + (uint32_t)(index), (__IO uint32_t *)tFamInfo.cFarmCode);
	index += 20;
	FLASH_ReadString(EEPROM_START_ADDRESS + (uint32_t)(index), (__IO uint32_t *)tFamInfo.cFarmName);
	index += 12;
	FLASH_ReadString(EEPROM_START_ADDRESS + (uint32_t)(index), (__IO uint32_t *)tFamInfo.cFwVersion);
	index += 12;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nDateTime_Hour);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nDateTime_Minute);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTimeUpload);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Ec);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_EcLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_ECHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_ECDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_WT);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_WTLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_WTHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_WTDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_PH);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_PHLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_PHHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_PHDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_Temp);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_TempLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_TempHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_TempDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_Humi);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_HumiLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_HumiHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_HumiDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_Light);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_LightLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_LightHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_Cli_LightDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_FlowRate);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_FlowRateLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_FlowRateHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_FlowRateDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_TankLevel);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_TankLevelLow);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_TankLevelHigh);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nThrehold_TankLevelDual);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nPump_TimePeri);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nPump_TimeNozzleOn);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nPump_TimeNozzleOff);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nPump_TimePumpMix);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ec1A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ec1B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ec2A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ec2B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Wt1A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Wt1B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Wt2A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Wt2B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ph1A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ph1B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ph2A);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nCalib_Ph2B);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Temp1);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Temp2);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Humi1);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Humi2);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Light1);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_Cli_Light2);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US1);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US2);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US3);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US4);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US5);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nOffset_US6);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nVoltage_Ph1_Newtral);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nVoltage_Ph1_Acid);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nVoltage_Ph2_Newtral);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nVoltage_Ph2_Acid);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nStandard_Ec);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nStandard_Ph);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_NozzleOn);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_NozzleOff);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_CoopadOn);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_CoopadOff);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_ChillerOn);
	index += 4;
	FLASH_Read(EEPROM_START_ADDRESS + (uint32_t)(index), &tFamInfo.nTime_ChillerOff);

	//HAL_FLASH_Lock();

	return true;
}

bool MODE_ParamsServerUpdate(void)
{
	sprintf(tFamInfo.cFarmCode, "%s", m_value[0].value);
	// sprintf(tFamInfo.cFarmName, "%s", m_value[1].value);
	// sprintf(tFamInfo.cFwVersion, "%s", m_value[2].value);
	strncpy(tFamInfo.cFarmName, m_value[1].value, 10);
	strncpy(tFamInfo.cFwVersion, m_value[2].value, 5);
	sprintf(tFamInfo.cDateTime, "%s", m_value[3].value);
	// implement parse datetime to hour, minute

	tFamInfo.nTimeUpload = atoi(m_value[4].value);
	tFamInfo.nThrehold_Ec = atoi(m_value[5].value);
	tFamInfo.nThrehold_EcLow = atoi(m_value[6].value);
	tFamInfo.nThrehold_ECHigh = atoi(m_value[7].value);
	tFamInfo.nThrehold_ECDual = atoi(m_value[8].value);
	tFamInfo.nThrehold_WT = atoi(m_value[9].value);
	tFamInfo.nThrehold_WTLow = atoi(m_value[10].value);
	tFamInfo.nThrehold_WTHigh = atoi(m_value[11].value);
	tFamInfo.nThrehold_WTDual = atoi(m_value[12].value);
	tFamInfo.nThrehold_PH = atoi(m_value[13].value);
	tFamInfo.nThrehold_PHLow = atoi(m_value[14].value);
	tFamInfo.nThrehold_PHHigh = atoi(m_value[15].value);
	tFamInfo.nThrehold_PHDual = atoi(m_value[16].value);
	tFamInfo.nThrehold_Cli_Temp = atoi(m_value[17].value);
	tFamInfo.nThrehold_Cli_TempLow = atoi(m_value[18].value);
	tFamInfo.nThrehold_Cli_TempHigh = atoi(m_value[19].value);
	tFamInfo.nThrehold_Cli_TempDual = atoi(m_value[20].value);
	tFamInfo.nThrehold_Cli_Humi = atoi(m_value[21].value);
	tFamInfo.nThrehold_Cli_HumiLow = atoi(m_value[22].value);
	tFamInfo.nThrehold_Cli_HumiHigh = atoi(m_value[23].value);
	tFamInfo.nThrehold_Cli_HumiDual = atoi(m_value[24].value);
	tFamInfo.nThrehold_Cli_Light = atoi(m_value[25].value);
	tFamInfo.nThrehold_Cli_LightLow = atoi(m_value[26].value);
	tFamInfo.nThrehold_Cli_LightHigh = atoi(m_value[27].value);
	tFamInfo.nThrehold_Cli_LightDual = atoi(m_value[28].value);
	tFamInfo.nThrehold_FlowRate = atoi(m_value[29].value);
	tFamInfo.nThrehold_FlowRateLow = atoi(m_value[30].value);
	tFamInfo.nThrehold_FlowRateHigh = atoi(m_value[31].value);
	tFamInfo.nThrehold_FlowRateDual = atoi(m_value[32].value);
	tFamInfo.nThrehold_TankLevel = atoi(m_value[33].value);
	tFamInfo.nThrehold_TankLevelLow = atoi(m_value[34].value);
	tFamInfo.nThrehold_TankLevelHigh = atoi(m_value[35].value);
	tFamInfo.nThrehold_TankLevelDual = atoi(m_value[36].value);
	tFamInfo.nPump_TimePeri = atoi(m_value[37].value);
	tFamInfo.nPump_TimeNozzleOn = atoi(m_value[38].value);
	tFamInfo.nPump_TimeNozzleOff = atoi(m_value[39].value);
	tFamInfo.nPump_TimePumpMix = atoi(m_value[40].value);
	tFamInfo.nCalib_Ec1A = atoi(m_value[41].value);
	tFamInfo.nCalib_Ec1B = atoi(m_value[42].value);
	tFamInfo.nCalib_Ec2A = atoi(m_value[43].value);
	tFamInfo.nCalib_Ec2B = atoi(m_value[44].value);
	tFamInfo.nCalib_Wt1A = atoi(m_value[45].value);
	tFamInfo.nCalib_Wt1B = atoi(m_value[46].value);
	tFamInfo.nCalib_Wt2A = atoi(m_value[47].value);
	tFamInfo.nCalib_Wt2B = atoi(m_value[48].value);
	tFamInfo.nCalib_Ph1A = atoi(m_value[49].value);
	tFamInfo.nCalib_Ph1B = atoi(m_value[50].value);
	tFamInfo.nCalib_Ph2A = atoi(m_value[51].value);
	tFamInfo.nCalib_Ph2B = atoi(m_value[52].value);
	tFamInfo.nOffset_Cli_Temp1 = atoi(m_value[53].value);
	tFamInfo.nOffset_Cli_Temp2 = atoi(m_value[54].value);
	tFamInfo.nOffset_Cli_Humi1 = atoi(m_value[55].value);
	tFamInfo.nOffset_Cli_Humi2 = atoi(m_value[56].value);
	tFamInfo.nOffset_Cli_Light1 = atoi(m_value[57].value);
	tFamInfo.nOffset_Cli_Light2 = atoi(m_value[58].value);
	tFamInfo.nOffset_US1 = atoi(m_value[59].value);
	tFamInfo.nOffset_US2 = atoi(m_value[60].value);
	tFamInfo.nOffset_US3 = atoi(m_value[61].value);
	tFamInfo.nOffset_US4 = atoi(m_value[62].value);
	tFamInfo.nOffset_US5 = atoi(m_value[63].value);
	tFamInfo.nOffset_US6 = atoi(m_value[64].value);
	tFamInfo.nVoltage_Ph1_Newtral = atoi(m_value[65].value);
	tFamInfo.nVoltage_Ph2_Newtral = atoi(m_value[66].value);
	tFamInfo.nVoltage_Ph1_Acid = atoi(m_value[67].value);
	tFamInfo.nVoltage_Ph2_Acid = atoi(m_value[68].value);
	tFamInfo.nTime_NozzleOn = atoi(m_value[69].value);
	tFamInfo.nTime_NozzleOff = atoi(m_value[70].value);
	tFamInfo.nTime_CoopadOn = atoi(m_value[71].value);
	tFamInfo.nTime_CoopadOff = atoi(m_value[72].value);
	tFamInfo.nTime_ChillerOn = atoi(m_value[73].value);
	tFamInfo.nTime_ChillerOff = atoi(m_value[74].value);
	tFamInfo.nTime_ShadingNetOn = atoi(m_value[75].value);
	tFamInfo.nTime_ShadingNetOff = atoi(m_value[76].value);

	return true;
}
void MODE_ConfigParameterBySoft(void){
	/* Mode Internet */
//	sprintf(tFamInfo.cFarmCode, "%s", "SGN000000IN000000");
//	sprintf(tFamInfo.cFarmName, "%s", "TOM");
//	sprintf(tFamInfo.cFwVersion, "%s", "IN01V");
	tFamInfo.nTimeUpload = 600;

	/* Mode Threhold */
	tFamInfo.nThrehold_Ec = 163;
	tFamInfo.nThrehold_EcLow = 160;
	tFamInfo.nThrehold_ECHigh = 165;
	tFamInfo.nThrehold_ECDual = 20;

	tFamInfo.nThrehold_WT = 250;
	tFamInfo.nThrehold_WTLow = 200;
	tFamInfo.nThrehold_WTHigh = 280;
	tFamInfo.nThrehold_WTDual = 20;

	tFamInfo.nThrehold_PH = 610;
	tFamInfo.nThrehold_PHLow = 590;
	tFamInfo.nThrehold_PHHigh = 630;
	tFamInfo.nThrehold_PHDual = 20;

	tFamInfo.nThrehold_Cli_Temp = 280;
	tFamInfo.nThrehold_Cli_TempLow = 200;
	tFamInfo.nThrehold_Cli_TempHigh = 320;
	tFamInfo.nThrehold_Cli_TempDual = 20;

	tFamInfo.nThrehold_Cli_Humi = 700;
	tFamInfo.nThrehold_Cli_HumiLow = 600;
	tFamInfo.nThrehold_Cli_HumiHigh = 800;
	tFamInfo.nThrehold_Cli_HumiDual = 50;

	tFamInfo.nThrehold_Cli_Light = 45000;
	tFamInfo.nThrehold_Cli_LightLow = 43000;
	tFamInfo.nThrehold_Cli_LightHigh = 48000;
	tFamInfo.nThrehold_Cli_LightDual = 2000;

	tFamInfo.nThrehold_FlowRate = 40;
	tFamInfo.nThrehold_FlowRateLow = 40;
	tFamInfo.nThrehold_FlowRateHigh = 40;
	tFamInfo.nThrehold_FlowRateDual = 40;

	tFamInfo.nThrehold_TankLevel = 500;
	tFamInfo.nThrehold_TankLevelLow = 200;
	tFamInfo.nThrehold_TankLevelHigh = 1200;
	tFamInfo.nThrehold_TankLevelDual = 50;

	/* Mode Pump */
	tFamInfo.nPump_TimePeri = 80;
	tFamInfo.nPump_TimeNozzleOn = 60;
	tFamInfo.nPump_TimeNozzleOff = 300;
	tFamInfo.nPump_TimePumpMix = 10;

	/* Mode Calib Ec */
	tFamInfo.nCalib_Ec1A = 560;
	tFamInfo.nCalib_Ec1B = 0;
	tFamInfo.nCalib_Ec2A = 543;
	tFamInfo.nCalib_Ec2B = 0;
	tFamInfo.nStandard_Ec = 1600;

	/* Mode Calib Water temperature*/
	tFamInfo.nCalib_Wt1A = 138;
	tFamInfo.nCalib_Wt1B = 0;
	tFamInfo.nCalib_Wt2A = 138;
	tFamInfo.nCalib_Wt2B = 0;

	/* Mode Calib Ph */
	tFamInfo.nStandard_Ph = 701;
	tFamInfo.nCalib_Ph1A = 108;
	tFamInfo.nCalib_Ph1B = 346;
	tFamInfo.nCalib_Ph2A = 108;
	tFamInfo.nCalib_Ph2B = 346;
	tFamInfo.nVoltage_Ph1_Newtral = 2000;
	tFamInfo.nVoltage_Ph2_Newtral = 2000;
	tFamInfo.nVoltage_Ph1_Acid = 500;
	tFamInfo.nVoltage_Ph2_Acid = 500;

	/* Mode Calib Climate Temp/Humi/Light */
	tFamInfo.nOffset_Cli_Temp1 = 0;
	tFamInfo.nOffset_Cli_Temp2 = 0;
	tFamInfo.nOffset_Cli_Humi1 = 0;
	tFamInfo.nOffset_Cli_Humi2 = 0;
	tFamInfo.nOffset_Cli_Light1 = 0;
	tFamInfo.nOffset_Cli_Light2 = 0;

	/* Mode Calib Tank Level */
	tFamInfo.nOffset_US1 = 0;
	tFamInfo.nOffset_US2 = 0;
	tFamInfo.nOffset_US3 = 0;
	tFamInfo.nOffset_US4 = 0;
	tFamInfo.nOffset_US5 = 0;
	tFamInfo.nOffset_US6 = 0;

	/* Mode ExIO */
	tFamInfo.nTime_NozzleOn = 8;
	tFamInfo.nTime_NozzleOff = 16;
	tFamInfo.nTime_CoopadOn = 9;
	tFamInfo.nTime_CoopadOff = 17;
	tFamInfo.nTime_ChillerOn = 9;
	tFamInfo.nTime_ChillerOff = 17;

	//			MODE_ParamSave();
	MODE_ParamsServerSave();
}
bool MODE_ParamsServerSave(void)
{
	int index = 0;

	HAL_FLASH_Unlock();
	FlashEraseSector(ADDR_FLASH_SECTOR_8,ADDR_FLASH_SECTOR_8  + 128 -1);

	FlashWriteString(EEPROM_START_ADDRESS + (uint32_t)(index), (uint32_t *)tFamInfo.cFarmCode);
	index += 20;
	FlashWriteString(EEPROM_START_ADDRESS + (uint32_t)(index), (uint32_t *)tFamInfo.cFarmName);
	index += 12;
	FlashWriteString(EEPROM_START_ADDRESS + (uint32_t)(index), (uint32_t *)tFamInfo.cFwVersion);
	index += 12;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nDateTime_Hour);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nDateTime_Minute);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTimeUpload);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Ec);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_EcLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_ECHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_ECDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_WT);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_WTLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_WTHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_WTDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_PH);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_PHLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_PHHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_PHDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_Temp);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_TempLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_TempHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_TempDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_Humi);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_HumiLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_HumiHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_HumiDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_Light);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_LightLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_LightHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_Cli_LightDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_FlowRate);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_FlowRateLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_FlowRateHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_FlowRateDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_TankLevel);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_TankLevelLow);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_TankLevelHigh);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nThrehold_TankLevelDual);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nPump_TimePeri);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nPump_TimeNozzleOn);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nPump_TimeNozzleOff);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nPump_TimePumpMix);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ec1A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ec1B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ec2A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ec2B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Wt1A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Wt1B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Wt2A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Wt2B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ph1A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ph1B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ph2A);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nCalib_Ph2B);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Temp1);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Temp2);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Humi1);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Humi2);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Light1);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_Cli_Light2);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US1);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US2);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US3);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US4);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US5);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nOffset_US6);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nVoltage_Ph1_Newtral);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nVoltage_Ph1_Acid);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nVoltage_Ph2_Newtral);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nVoltage_Ph2_Acid);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nStandard_Ec);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nStandard_Ph);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_NozzleOn);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_NozzleOff);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_CoopadOn);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_CoopadOff);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_ChillerOn);
	index += 4;
	FlashWriteWord(EEPROM_START_ADDRESS + (uint32_t)(index), (int32_t)tFamInfo.nTime_ChillerOff);

	HAL_FLASH_Lock();

	return true;
}
