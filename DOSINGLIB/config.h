#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "customer.h"

#define INTERNAL_FLASH	// uncomment this for using Internal Flash/ comment this for using W25Q16 flash
//#define TEST_MODE
//#define EEPROM_START_ADDRESS ((uint32_t)0x0807F800)
#define EEPROM_START_ADDRESS ADDR_FLASH_SECTOR_8

/* FarmCode */
//#define farmCode	FC_INDUS_TEST1
//#define farmName	FN_INDUS_TEST1

/* 
IN01B
IN02B
*/
//#define FwVersion	"IN01B"
//#define IN02B


#endif
