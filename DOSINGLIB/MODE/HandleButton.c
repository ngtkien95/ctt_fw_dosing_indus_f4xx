/*
 * HandleButton.c
 *
 *  Created on: Oct 10, 2021
 *      Author: Zeder
 */


#include "../include.h"

extern _DS3231 tRtc;
extern _HCSR04 tDistance;
//extern _ETHW5500 ethCheck;
extern _SENSOR_IRRI tSensorIrriValue;
extern _SENSOR_CLI tSensorCliValue;
//extern Value_t m_value[100];
extern Value_t m_value[100];
extern _FAM_INFO tFamInfo;

extern int16_t nTimeRefreshLcd;
extern uint16_t nTimerCheckDebounce;
extern float fWaterTempStandard;
extern float fClimateTempStandard;
extern float fClimateHumiStandard;
extern uint16_t nClimateLightStandard;
extern uint16_t nTankLevelStandard1;
extern uint16_t nTankLevelStandard2;
extern uint16_t nTankLevelStandard3;
extern bool refreshLcd;
extern bool success;
extern bool bZigbeeState;
extern char ac_BuffLcd[30];
extern flagMode_t flagMode;
extern modeDisplay_t modeDisplay;
extern modeSetting_t modeSetting;
extern settingEthernet_t settingEthernet;
extern settingThreshold_t settingThreshold;
extern settingRtc_t settingRtc;
extern settingPump_t settingPump;
extern settingCalibEC_t settingCalibEC;
extern settingCalibWT_t settingCalibWT;
extern settingCalibPH_t settingCalibPH;
extern settingCalibTE_t settingCalibTE;
extern settingCalibHU_t settingCalibHU;
extern settingCalibLI_t settingCalibLI;
extern settingCalibFS_t settingCalibFS;
extern settingCalibTankLevel1_t settingCalibTankLevel1;
extern settingCalibTankLevel2_t settingCalibTankLevel2;
extern settingCalibTankLevel3_t settingCalibTankLevel3;
extern settingExIO_t settingExIO;
extern settingEnable_t settingEnable;
extern settingInfo_t settingInfo;


bool HandleButtonMode(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		flagMode = MODE_SETTING;
		modeSetting = SETTING_INTERNET;
		settingEthernet = SETTING_ETHERNET_NONE;
		settingThreshold = SETTING_THRESHOLD_NONE;
		settingPump = SETTING_PUMP_NONE;
		settingCalibEC = SETTING_CALIB_EC_NONE;
		settingCalibWT = SETTING_CALIB_WT_NONE;
		settingCalibPH = SETTING_CALIB_PH_NONE;
		settingCalibTE = SETTING_CALIB_TE_NONE;
		settingCalibHU = SETTING_CALIB_HU_NONE;
		settingCalibLI = SETTING_CALIB_LI_NONE;
		settingCalibFS = SETTING_CALIB_FS_NONE;
		settingCalibTankLevel1 = SETTING_CALIB_TANK_LEVEL_1_NONE;
		settingCalibTankLevel2 = SETTING_CALIB_TANK_LEVEL_2_NONE;
		settingCalibTankLevel3 = SETTING_CALIB_TANK_LEVEL_3_NONE;
		settingExIO = SETTING_EXIO_NONE;
		settingRtc = SETTING_RTC_NONE;
		settingEnable = SETTING_ENABLE_NONE;
		settingInfo = SETTING_INFO_NONE;
		refreshLcd = true;
		break;
	case MODE_SETTING:
		if ((settingEthernet == SETTING_ETHERNET_NONE) && (settingThreshold == SETTING_THRESHOLD_NONE) &&
			(settingPump == SETTING_PUMP_NONE) && (settingCalibEC == SETTING_CALIB_EC_NONE) &&
			(settingCalibWT == SETTING_CALIB_WT_NONE) && (settingCalibPH == SETTING_CALIB_PH_NONE) &&
			(settingCalibTE == SETTING_CALIB_TE_NONE) && (settingCalibHU == SETTING_CALIB_HU_NONE) &&
			(settingCalibLI == SETTING_CALIB_LI_NONE) && (settingCalibFS == SETTING_CALIB_FS_NONE) &&
			(settingCalibTankLevel1 == SETTING_CALIB_TANK_LEVEL_1_NONE) && (settingCalibTankLevel2 == SETTING_CALIB_TANK_LEVEL_2_NONE) &&
			(settingCalibTankLevel3 == SETTING_CALIB_TANK_LEVEL_3_NONE) && (settingExIO == SETTING_EXIO_NONE) &&
			(settingRtc == SETTING_RTC_NONE) && (settingEnable == SETTING_ENABLE_NONE) &&
			(settingInfo == SETTING_INFO_NONE))
		{
			modeSetting = SETTING_INTERNET;
			flagMode = MODE_DISPLAY;
		}
		else if (settingRtc != SETTING_RTC_NONE)
		{
			tRtc.State = DS3231_SET_TIME;
			settingRtc = SETTING_RTC_NONE;
			MODE_Disp_Success();
		}
		else if (settingInfo != SETTING_INFO_NONE)
		{
			settingInfo = SETTING_INFO_NONE;
		}
		else if (settingEnable != SETTING_ENABLE_NONE)
		{
			settingEnable = SETTING_ENABLE_NONE;
		}
		else
		{
			MODE_ParamsServerSave();
			MODE_Disp_Success();
			//ETH_ResetTimerSysInfo();
		}
		refreshLcd = true;
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}

	return true;
}
bool HandleButtonSet(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_INTERNET:
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				settingEthernet = SETTING_ZIGBEE_STATE;
				refreshLcd = true;
				break;
			case SETTING_ZIGBEE_STATE:
				settingEthernet = SETTING_ETHERNET_UPDATE;
				break;
			case SETTING_ETHERNET_UPDATE:
				settingEthernet = SETTING_ZIGBEE_STATE;
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				settingRtc = SETTING_RTC_HOUR;
				refreshLcd = true;
				break;
			case SETTING_RTC_HOUR:
				settingRtc = SETTING_RTC_MINUTE;
				break;
			case SETTING_RTC_MINUTE:
				settingRtc = SETTING_RTC_HOUR;
				break;
			default:
				break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				settingThreshold = SETTING_THRESHOLD_EC;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_EC:
				settingThreshold = SETTING_THRESHOLD_EC_LOW;
				break;
			case SETTING_THRESHOLD_EC_LOW:
				settingThreshold = SETTING_THRESHOLD_EC_HIGH;
				break;
			case SETTING_THRESHOLD_EC_HIGH:
				settingThreshold = SETTING_THRESHOLD_EC_DUAL;
				break;
			case SETTING_THRESHOLD_EC_DUAL:
				settingThreshold = SETTING_THRESHOLD_WATERTEMP;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_WATERTEMP:
				settingThreshold = SETTING_THRESHOLD_WATERTEMP_LOW;
				break;
			case SETTING_THRESHOLD_WATERTEMP_LOW:
				settingThreshold = SETTING_THRESHOLD_WATERTEMP_HIGH;
				break;
			case SETTING_THRESHOLD_WATERTEMP_HIGH:
				settingThreshold = SETTING_THRESHOLD_WATERTEMP_DUAL;
				break;
			case SETTING_THRESHOLD_WATERTEMP_DUAL:
				settingThreshold = SETTING_THRESHOLD_PH;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_PH:
				settingThreshold = SETTING_THRESHOLD_PH_LOW;
				break;
			case SETTING_THRESHOLD_PH_LOW:
				settingThreshold = SETTING_THRESHOLD_PH_HIGH;
				break;
			case SETTING_THRESHOLD_PH_HIGH:
				settingThreshold = SETTING_THRESHOLD_PH_DUAL;
				break;
			case SETTING_THRESHOLD_PH_DUAL:
				settingThreshold = SETTING_THRESHOLD_TE;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_TE:
				settingThreshold = SETTING_THRESHOLD_TE_LOW;
				break;
			case SETTING_THRESHOLD_TE_LOW:
				settingThreshold = SETTING_THRESHOLD_TE_HIGH;
				break;
			case SETTING_THRESHOLD_TE_HIGH:
				settingThreshold = SETTING_THRESHOLD_TE_DUAL;
				break;
			case SETTING_THRESHOLD_TE_DUAL:
				settingThreshold = SETTING_THRESHOLD_HU;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_HU:
				settingThreshold = SETTING_THRESHOLD_HU_LOW;
				break;
			case SETTING_THRESHOLD_HU_LOW:
				settingThreshold = SETTING_THRESHOLD_HU_HIGH;
				break;
			case SETTING_THRESHOLD_HU_HIGH:
				settingThreshold = SETTING_THRESHOLD_HU_DUAL;
				break;
			case SETTING_THRESHOLD_HU_DUAL:
				settingThreshold = SETTING_THRESHOLD_LI;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_LI:
				settingThreshold = SETTING_THRESHOLD_LI_LOW;
				break;
			case SETTING_THRESHOLD_LI_LOW:
				settingThreshold = SETTING_THRESHOLD_LI_HIGH;
				break;
			case SETTING_THRESHOLD_LI_HIGH:
				settingThreshold = SETTING_THRESHOLD_LI_DUAL;
				break;
			case SETTING_THRESHOLD_LI_DUAL:
				settingThreshold = SETTING_THRESHOLD_FS;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_FS:
				settingThreshold = SETTING_THRESHOLD_FS_LOW;
				break;
			case SETTING_THRESHOLD_FS_LOW:
				settingThreshold = SETTING_THRESHOLD_FS_HIGH;
				break;
			case SETTING_THRESHOLD_FS_HIGH:
				settingThreshold = SETTING_THRESHOLD_FS_DUAL;
				break;
			case SETTING_THRESHOLD_FS_DUAL:
				settingThreshold = SETTING_THRESHOLD_TANKLEVEL;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_TANKLEVEL:
				settingThreshold = SETTING_THRESHOLD_TANKLEVEL_LOW;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_LOW:
				settingThreshold = SETTING_THRESHOLD_TANKLEVEL_HIGH;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_HIGH:
				settingThreshold = SETTING_THRESHOLD_TANKLEVEL_DUAL;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_DUAL:
				settingThreshold = SETTING_THRESHOLD_EC;
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_PUMP:
			switch (settingPump)
			{
			case SETTING_PUMP_NONE:
				settingPump = SETTING_TIME_PUMP_PERI;
				refreshLcd = true;
				break;
			case SETTING_TIME_PUMP_PERI:
				settingPump = SETTING_TIME_PUMP_MIX;
				break;
			case SETTING_TIME_PUMP_MIX:
				settingPump = SETTING_TIME_PUMP_NOZZLE;
				break;
			case SETTING_TIME_PUMP_NOZZLE:
				settingPump = SETTING_TIME_PUMP_NOZZLE_WAIT;
				break;
			case SETTING_TIME_PUMP_NOZZLE_WAIT:
				settingPump = SETTING_TIME_PUMP_PERI;
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				settingCalibEC = SETTING_EC_STANDARD;
				refreshLcd = true;
				break;
			case SETTING_EC_STANDARD:
				settingCalibEC = SETTING_CLICK_CALIB_EC;
				break;
			case SETTING_CLICK_CALIB_EC:
				settingCalibEC = SETTING_CALIB_EC1_A;
				refreshLcd = true;
				break;
			case SETTING_CALIB_EC1_A:
				settingCalibEC = SETTING_CALIB_EC1_B;
				break;
			case SETTING_CALIB_EC1_B:
				settingCalibEC = SETTING_CALIB_EC2_A;
				break;
			case SETTING_CALIB_EC2_A:
				settingCalibEC = SETTING_CALIB_EC2_B;
				break;
			case SETTING_CALIB_EC2_B:
				settingCalibEC = SETTING_EC_STANDARD;
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				fWaterTempStandard = tSensorIrriValue.dWtAvg;
				settingCalibWT = SETTING_WT_STANDARD;
				refreshLcd = true;
				break;
			case SETTING_WT_STANDARD:
				settingCalibWT = SETTING_CLICK_CALIB_WT;
				break;
			case SETTING_CLICK_CALIB_WT:
				settingCalibWT = SETTING_WT_STANDARD;
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				settingCalibPH = SETTING_PH_STANDARD;
				refreshLcd = true;
				break;
			case SETTING_PH_STANDARD:
				settingCalibPH = SETTING_CLICK_CALIB_PH;
				break;
			case SETTING_CLICK_CALIB_PH:
				settingCalibPH = SETTING_CALIB_PH1_A;
				refreshLcd = true;
				break;
			case SETTING_CALIB_PH1_A:
				settingCalibPH = SETTING_CALIB_PH1_B;
				break;
			case SETTING_CALIB_PH1_B:
				settingCalibPH = SETTING_CALIB_PH2_A;
				break;
			case SETTING_CALIB_PH2_A:
				settingCalibPH = SETTING_CALIB_PH2_B;
				break;
			case SETTING_CALIB_PH2_B:
				settingCalibPH = SETTING_PH_STANDARD;
				break;
			}
			break;
		case SETTING_CALIB_TE:
			switch (settingCalibTE)
			{
			case SETTING_CALIB_TE_NONE:
				fClimateTempStandard = tSensorCliValue.fCliTempAvg;
				settingCalibTE = SETTING_CALIB_TE_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TE_STAND:
				break;
			}
			break;
		case SETTING_CALIB_HU:
			switch (settingCalibHU)
			{
			case SETTING_CALIB_HU_NONE:
				fClimateHumiStandard = tSensorCliValue.fCliHumiAvg;
				settingCalibHU = SETTING_CALIB_HU_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_HU_STAND:
				break;
			}
			break;
		case SETTING_CALIB_LI:
			switch (settingCalibLI)
			{
			case SETTING_CALIB_LI_NONE:
				nClimateLightStandard = tSensorCliValue.nCliLightAvg;
				settingCalibLI = SETTING_CALIB_LI_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_LI_STAND:
				break;
			}
			break;
		case SETTING_CALIB_FS:
			switch (settingCalibFS)
			{
			case SETTING_CALIB_FS_NONE:
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_1:
			switch (settingCalibTankLevel1)
			{
			case SETTING_CALIB_TANK_LEVEL_1_NONE:
				nTankLevelStandard1 = tDistance.dLitTank1;
				settingCalibTankLevel1 = SETTING_CALIB_TANK_LEVEL_1_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TANK_LEVEL_1_STAND:
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_2:
			switch (settingCalibTankLevel2)
			{
			case SETTING_CALIB_TANK_LEVEL_2_NONE:
				nTankLevelStandard2 = tDistance.dLitTank2;
				settingCalibTankLevel2 = SETTING_CALIB_TANK_LEVEL_2_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TANK_LEVEL_2_STAND:
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_3:
			switch (settingCalibTankLevel3)
			{
			case SETTING_CALIB_TANK_LEVEL_3_NONE:
				nTankLevelStandard3 = tDistance.dLitTank3;
				settingCalibTankLevel3 = SETTING_CALIB_TANK_LEVEL_3_STAND;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TANK_LEVEL_3_STAND:
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingExIO)
			{
			case SETTING_EXIO_NONE:
				settingExIO = SETTING_EXIO_NOZZLE_TIME_ON;
				refreshLcd = true;
				break;
			case SETTING_EXIO_NOZZLE_TIME_ON:
				settingExIO = SETTING_EXIO_NOZZLE_TIME_OFF;
				break;
			case SETTING_EXIO_NOZZLE_TIME_OFF:
				settingExIO = SETTING_EXIO_COOPAD_TIME_ON;
				break;
			case SETTING_EXIO_COOPAD_TIME_ON:
				settingExIO = SETTING_EXIO_COOPAD_TIME_OFF;
				break;
			case SETTING_EXIO_COOPAD_TIME_OFF:
				settingExIO = SETTING_EXIO_NOZZLE_TIME_ON;
				break;
			}
			break;
		case SETTING_ENABLE:
			switch (settingEnable)
			{
			case SETTING_ENABLE_NONE:
				settingEnable = SETTING_ENABLE_EC;
				refreshLcd = true;
				break;
			case SETTING_ENABLE_EC:
				settingEnable = SETTING_ENABLE_PH;
				break;
			case SETTING_ENABLE_PH:
				settingEnable = SETTING_ENABLE_O1;
				break;
			case SETTING_ENABLE_O1:
				settingEnable = SETTING_ENABLE_O2;
				break;
			case SETTING_ENABLE_O2:
				settingEnable = SETTING_ENABLE_O3;
				refreshLcd = true;
				break;
			case SETTING_ENABLE_O3:
				settingEnable = SETTING_ENABLE_O4;
				break;
			case SETTING_ENABLE_O4:
				settingEnable = SETTING_ENABLE_O5;
				break;
			case SETTING_ENABLE_O5:
				settingEnable = SETTING_ENABLE_O6;
				break;
			case SETTING_ENABLE_O6:
				settingEnable = SETTING_ENABLE_O7;
				refreshLcd = true;
				break;
			case SETTING_ENABLE_O7:
				settingEnable = SETTING_ENABLE_O8;
				break;
			case SETTING_ENABLE_O8:
				settingEnable = SETTING_ENABLE_O9;
				break;
			case SETTING_ENABLE_O9:
				settingEnable = SETTING_ENABLE_O10;
				break;
			case SETTING_ENABLE_O10:
				settingEnable = SETTING_ENABLE_EC;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			if (nTimerCheckDebounce > 10000)
			{
				//TODO: Setting default
				/* Info */
//				MODE_ParamSave();
//				MODE_ParamsServerSave();
				//ETH_ResetTimerSysInfo();
				sprintf(tFamInfo.cFarmCode, "%s", "SGN000000IN000000");
			    sprintf(tFamInfo.cFarmName, "%s", "TOM");
				sprintf(tFamInfo.cFwVersion, "%s", "IN01V");
				MODE_ConfigParameterBySoft();

				flagMode = MODE_DISPLAY;
				refreshLcd = true;
			}
			else
			{
				/* Mode Internet */
				MODE_ConfigParameterBySoft();

				//			MODE_ParamSave();
				MODE_ParamsServerSave();
				//ETH_ResetTimerSysInfo();
				flagMode = MODE_DISPLAY;
				refreshLcd = true;
			}
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				settingInfo = SETTING_INFO_FWVERSION;
				refreshLcd = true;
				break;
			case SETTING_INFO_FWVERSION:
				break;
			case SETTING_INFO_FARMCODE:
				break;
			}
			break;
//		default:
//			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}

	return true;
}
bool HandleButtonUp(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		switch (modeDisplay)
		{
		case DISPLAY_EC_PH_WT:
			modeDisplay = DISPLAY_TE_HU_LI;
			refreshLcd = true;
			break;
		case DISPLAY_TE_HU_LI:
			modeDisplay = DISPLAY_FS_US;
			refreshLcd = true;
			break;
		case DISPLAY_FS_US:
			modeDisplay = DISPLAY_EC_PH_WT;
			refreshLcd = true;
			break;
		}
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_INTERNET:

			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				modeSetting = SETTING_INFO;
				refreshLcd = true;
				break;
			case SETTING_ZIGBEE_STATE:
//				if (bZigbeeState == true)
//				{
//					bZigbeeState = false;
//				}
//				else
//				{
//					bZigbeeState = true;
//				}
				break;
			case SETTING_ETHERNET_UPDATE:
//				tFamInfo.nTimeUpload++;
				break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				modeSetting = SETTING_INTERNET;
				break;
			case SETTING_THRESHOLD_EC:
				tFamInfo.nThrehold_Ec++;
				if (tFamInfo.nThrehold_Ec <= tFamInfo.nThrehold_EcLow){
					tFamInfo.nThrehold_Ec = tFamInfo.nThrehold_EcLow;
				}else if (tFamInfo.nThrehold_Ec >= tFamInfo.nThrehold_ECHigh){
					tFamInfo.nThrehold_Ec = tFamInfo.nThrehold_ECHigh;
				}
				break;
			case SETTING_THRESHOLD_EC_LOW:
				if (tFamInfo.nThrehold_EcLow < tFamInfo.nThrehold_ECHigh)
					tFamInfo.nThrehold_EcLow++;
				break;
			case SETTING_THRESHOLD_EC_HIGH:
				tFamInfo.nThrehold_ECHigh++;
				break;
			case SETTING_THRESHOLD_EC_DUAL:
				tFamInfo.nThrehold_ECDual++;
				break;
			case SETTING_THRESHOLD_WATERTEMP:
				tFamInfo.nThrehold_WT += 10;
				if (tFamInfo.nThrehold_WT <= tFamInfo.nThrehold_WTLow){
					tFamInfo.nThrehold_WT = tFamInfo.nThrehold_WTLow;
				}else if (tFamInfo.nThrehold_WT <= tFamInfo.nThrehold_WTHigh){
					tFamInfo.nThrehold_WT = tFamInfo.nThrehold_WTHigh;
				}
				break;
			case SETTING_THRESHOLD_WATERTEMP_LOW:
				if (tFamInfo.nThrehold_WTLow < tFamInfo.nThrehold_WTHigh)
					tFamInfo.nThrehold_WTLow += 10;
				break;
			case SETTING_THRESHOLD_WATERTEMP_HIGH:
				tFamInfo.nThrehold_WTHigh += 10;
				break;
			case SETTING_THRESHOLD_WATERTEMP_DUAL:
				tFamInfo.nThrehold_WTDual += 10;
				break;
			case SETTING_THRESHOLD_PH:
				tFamInfo.nThrehold_PH++;
				if (tFamInfo.nThrehold_PH <= tFamInfo.nThrehold_PHLow){
					tFamInfo.nThrehold_PH = tFamInfo.nThrehold_PHLow;
				}else if (tFamInfo.nThrehold_PH >= tFamInfo.nThrehold_PHHigh){
					tFamInfo.nThrehold_PH = tFamInfo.nThrehold_PHHigh;
				}
				break;
			case SETTING_THRESHOLD_PH_LOW:
				if (tFamInfo.nThrehold_PHLow < tFamInfo.nThrehold_PHHigh)
					tFamInfo.nThrehold_PHLow++;
				break;
			case SETTING_THRESHOLD_PH_HIGH:
				tFamInfo.nThrehold_PHHigh++;
				break;
			case SETTING_THRESHOLD_PH_DUAL:
				tFamInfo.nThrehold_PHDual++;
				break;
			case SETTING_THRESHOLD_TE:
				tFamInfo.nThrehold_Cli_Temp += 10;
				if (tFamInfo.nThrehold_Cli_Temp <= tFamInfo.nThrehold_Cli_TempLow){
					tFamInfo.nThrehold_Cli_Temp = tFamInfo.nThrehold_Cli_TempLow;
        }else if (tFamInfo.nThrehold_Cli_Temp >= tFamInfo.nThrehold_Cli_TempHigh){
					tFamInfo.nThrehold_Cli_Temp = tFamInfo.nThrehold_Cli_TempHigh;
				}
				break;
			case SETTING_THRESHOLD_TE_LOW:
				if (tFamInfo.nThrehold_Cli_TempLow < tFamInfo.nThrehold_Cli_TempHigh)
					tFamInfo.nThrehold_Cli_TempLow += 10;
				break;
			case SETTING_THRESHOLD_TE_HIGH:
				tFamInfo.nThrehold_Cli_TempHigh += 10;
				break;
			case SETTING_THRESHOLD_TE_DUAL:
				tFamInfo.nThrehold_Cli_TempDual += 10;
				break;
			case SETTING_THRESHOLD_HU:
				tFamInfo.nThrehold_Cli_Humi += 10;
				if (tFamInfo.nThrehold_Cli_Humi <= tFamInfo.nThrehold_Cli_HumiLow){
					tFamInfo.nThrehold_Cli_Humi = tFamInfo.nThrehold_Cli_HumiLow;
				}else if (tFamInfo.nThrehold_Cli_Humi >= tFamInfo.nThrehold_Cli_HumiHigh){
					tFamInfo.nThrehold_Cli_Humi = tFamInfo.nThrehold_Cli_HumiHigh;
				}
				break;
			case SETTING_THRESHOLD_HU_LOW:
				if (tFamInfo.nThrehold_Cli_HumiLow < tFamInfo.nThrehold_Cli_HumiHigh)
					tFamInfo.nThrehold_Cli_HumiLow += 10;
				break;
			case SETTING_THRESHOLD_HU_HIGH:
				tFamInfo.nThrehold_Cli_HumiHigh += 10;
				break;
			case SETTING_THRESHOLD_HU_DUAL:
				tFamInfo.nThrehold_Cli_HumiDual += 10;
				break;
			case SETTING_THRESHOLD_LI:
				tFamInfo.nThrehold_Cli_Light += 1000;
				if (tFamInfo.nThrehold_Cli_Light <= tFamInfo.nThrehold_Cli_LightLow){
					tFamInfo.nThrehold_Cli_Light = tFamInfo.nThrehold_Cli_LightLow;
				}else if (tFamInfo.nThrehold_Cli_Light >= tFamInfo.nThrehold_Cli_LightHigh){
					tFamInfo.nThrehold_Cli_Light = tFamInfo.nThrehold_Cli_LightHigh;
				}
				break;
			case SETTING_THRESHOLD_LI_LOW:
				if (tFamInfo.nThrehold_Cli_LightLow < tFamInfo.nThrehold_Cli_LightHigh)
					tFamInfo.nThrehold_Cli_LightLow += 1000;
				break;
			case SETTING_THRESHOLD_LI_HIGH:
				tFamInfo.nThrehold_Cli_LightHigh += 1000;
				break;
			case SETTING_THRESHOLD_LI_DUAL:
				tFamInfo.nThrehold_Cli_LightDual += 1000;
				break;
			case SETTING_THRESHOLD_FS:
				tFamInfo.nThrehold_FlowRate++;
				break;
			case SETTING_THRESHOLD_FS_LOW:
				tFamInfo.nThrehold_FlowRateLow++;
				break;
			case SETTING_THRESHOLD_FS_HIGH:
				tFamInfo.nThrehold_FlowRateHigh++;
				break;
			case SETTING_THRESHOLD_FS_DUAL:
				tFamInfo.nThrehold_FlowRateDual++;
				break;
			case SETTING_THRESHOLD_TANKLEVEL:
				tFamInfo.nThrehold_TankLevel += 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_LOW:
				if (tFamInfo.nThrehold_TankLevelLow < tFamInfo.nThrehold_TankLevel)
					tFamInfo.nThrehold_TankLevelLow += 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_HIGH:
				tFamInfo.nThrehold_TankLevelHigh += 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_DUAL:
				tFamInfo.nThrehold_TankLevelDual += 10;
				break;
			}
			break;
		case SETTING_PUMP:
			switch (settingPump)
			{
			case SETTING_PUMP_NONE:
				modeSetting = SETTING_THRESHOLD;
				break;
			case SETTING_TIME_PUMP_PERI:
				tFamInfo.nPump_TimePeri++;
				break;
			case SETTING_TIME_PUMP_MIX:
				tFamInfo.nPump_TimePumpMix++;
				break;
			case SETTING_TIME_PUMP_NOZZLE:
				tFamInfo.nPump_TimeNozzleOn++;
				break;
			case SETTING_TIME_PUMP_NOZZLE_WAIT:
				tFamInfo.nPump_TimeNozzleOff++;
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				modeSetting = SETTING_PUMP;
				refreshLcd = true;
				break;
			case SETTING_EC_STANDARD:
				tFamInfo.nStandard_Ec++;
				if (tFamInfo.nStandard_Ec <= 0){
					tFamInfo.nStandard_Ec = 0;
				}else if (tFamInfo.nStandard_Ec >= 5000){
					tFamInfo.nStandard_Ec = 5000;
				}
//				switch (tFamInfo.nStandard_Ec)
//				{
//				case 1000:
//					tFamInfo.nStandard_Ec = 1413;
//					break;
//				case 1413:
//					tFamInfo.nStandard_Ec = 2000;
//					break;
//				case 2000:
//					tFamInfo.nStandard_Ec = 5000;
//					break;
//				case 5000:
//					tFamInfo.nStandard_Ec = 1000;
//					break;
//				default:
//					break;
//				}
				break;
			case SETTING_CLICK_CALIB_EC:
				SENSOR_CalibEc(tFamInfo.nStandard_Ec, &tFamInfo.nCalib_Ec1A, &tFamInfo.nCalib_Ec2A);
				break;
			case SETTING_CALIB_EC1_A:
				tFamInfo.nCalib_Ec1A++;
				break;
			case SETTING_CALIB_EC1_B:
				tFamInfo.nCalib_Ec1B++;
				break;
			case SETTING_CALIB_EC2_A:
				tFamInfo.nCalib_Ec2A++;
				break;
			case SETTING_CALIB_EC2_B:
				tFamInfo.nCalib_Ec2B++;
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				modeSetting = SETTING_CALIB_EC;
				break;
			case SETTING_WT_STANDARD:
				fWaterTempStandard += 0.1;
				break;
			case SETTING_CLICK_CALIB_WT:
				SENSOR_CalibWt(fWaterTempStandard);
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				modeSetting = SETTING_CALIB_WT;
				break;
			case SETTING_PH_STANDARD:
				switch (tFamInfo.nStandard_Ph)
				{
				case 401:
					tFamInfo.nStandard_Ph = 701;
					break;
				case 701:
					tFamInfo.nStandard_Ph = 401;
					break;
				default:
					break;
				}
				break;
			case SETTING_CLICK_CALIB_PH:
				SENSOR_CalibPh(tFamInfo.nStandard_Ph, &tFamInfo.nCalib_Ph1A, &tFamInfo.nCalib_Ph1B, &tFamInfo.nCalib_Ph2A, &tFamInfo.nCalib_Ph2B);
				break;
			case SETTING_CALIB_PH1_A:
				tFamInfo.nCalib_Ph1A++;
				break;
			case SETTING_CALIB_PH1_B:
				tFamInfo.nCalib_Ph1B++;
				break;
			case SETTING_CALIB_PH2_A:
				tFamInfo.nCalib_Ph2A++;
				break;
			case SETTING_CALIB_PH2_B:
				tFamInfo.nCalib_Ph2B++;
				break;
			}
			break;
		case SETTING_CALIB_TE:
			switch (settingCalibTE)
			{
			case SETTING_CALIB_TE_NONE:
				modeSetting = SETTING_CALIB_PH;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TE_STAND:
				fClimateTempStandard += 0.1;
				SENSOR_CalibTemp(fClimateTempStandard);
				break;
			}
			break;
		case SETTING_CALIB_HU:
			switch (settingCalibHU)
			{
			case SETTING_CALIB_HU_NONE:
				modeSetting = SETTING_CALIB_TE;
				break;
			case SETTING_CALIB_HU_STAND:
				fClimateHumiStandard += 1;
				SENSOR_CalibHumi(fClimateHumiStandard);
				break;
			}
			break;
		case SETTING_CALIB_LI:
			switch (settingCalibLI)
			{
			case SETTING_CALIB_LI_NONE:
				modeSetting = SETTING_CALIB_HU;
				break;
			case SETTING_CALIB_LI_STAND:
				nClimateLightStandard += 100;
				SENSOR_CalibLight(nClimateLightStandard);
				break;
			}
			break;
		case SETTING_CALIB_FS:
			switch (settingCalibFS)
			{
			case SETTING_CALIB_FS_NONE:
				modeSetting = SETTING_CALIB_LI;
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_1:
			switch (settingCalibTankLevel1)
			{
			case SETTING_CALIB_TANK_LEVEL_1_NONE:
				modeSetting = SETTING_CALIB_FS;
				break;
			case SETTING_CALIB_TANK_LEVEL_1_STAND:
				nTankLevelStandard1 += 1;
				HCSR04_calibTank1(nTankLevelStandard1);
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_2:
			switch (settingCalibTankLevel2)
			{
			case SETTING_CALIB_TANK_LEVEL_2_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_1;
				break;
			case SETTING_CALIB_TANK_LEVEL_2_STAND:
				nTankLevelStandard2 += 1;
				HCSR04_calibTank2(nTankLevelStandard2);
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_3:
			switch (settingCalibTankLevel3)
			{
			case SETTING_CALIB_TANK_LEVEL_3_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_2;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TANK_LEVEL_3_STAND:
				nTankLevelStandard3 += 1;
				HCSR04_calibTank3(nTankLevelStandard3);
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingExIO)
			{
			case SETTING_EXIO_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_3;
				break;
			case SETTING_EXIO_NOZZLE_TIME_ON:
				tFamInfo.nTime_NozzleOn++;
				if (tFamInfo.nTime_NozzleOn >= 12)
					tFamInfo.nTime_NozzleOn = 12;
				else if (tFamInfo.nTime_NozzleOn <= 0)
					tFamInfo.nTime_NozzleOn = 0;
				break;
			case SETTING_EXIO_NOZZLE_TIME_OFF:
				tFamInfo.nTime_NozzleOff++;
				if (tFamInfo.nTime_NozzleOff >= 23)
					tFamInfo.nTime_NozzleOff = 23;
				else if (tFamInfo.nTime_NozzleOff <= 12)
					tFamInfo.nTime_NozzleOff = 12;
				break;
			case SETTING_EXIO_COOPAD_TIME_ON:
				tFamInfo.nTime_CoopadOn++;
				if (tFamInfo.nTime_CoopadOn >= 12)
					tFamInfo.nTime_CoopadOn = 12;
				else if (tFamInfo.nTime_CoopadOn <= 0)
					tFamInfo.nTime_CoopadOn = 0;
				break;
			case SETTING_EXIO_COOPAD_TIME_OFF:
				tFamInfo.nTime_CoopadOff++;
				if (tFamInfo.nTime_CoopadOff >= 23)
					tFamInfo.nTime_CoopadOff = 23;
				else if (tFamInfo.nTime_CoopadOff <= 12)
					tFamInfo.nTime_CoopadOff = 12;
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				modeSetting = SETTING_EXIO;
				break;
			case SETTING_RTC_HOUR:
				//Increase Hour
				tRtc.Hour++;
				if (tRtc.Hour > 23)
					tRtc.Hour = 0;
				else if (tRtc.Hour < 0)
					tRtc.Hour = 23;
				break;
			case SETTING_RTC_MINUTE:
				//Increase Minute
				tRtc.Min++;
				if (tRtc.Min > 59)
					tRtc.Min = 0;
				else if (tRtc.Min < 0)
					tRtc.Min = 59;
				break;
			}
			break;
		case SETTING_ENABLE:
			/*
					O1 - PD0 - OP_SOLENOID_NUTRI
					O2 - PD1 - OP_SOLENOID_WATER
					O3 - PD2 - OP_PMIX
					O4 - PD3 - OP_NOZZLE
					O5 - PD4 - OP_COOLINGPAD
					O6 - PD5 - OP_FAN
					O7 - PB9 - ACT4
					O8 - PE1 - ACT3
					O9 - PE0 - ACT2
					O10 - PD6 - ACT1
				*/
			switch (settingEnable)
			{
			case SETTING_ENABLE_NONE:
				modeSetting = SETTING_RTC;
				refreshLcd = true;
				break;
			case SETTING_ENABLE_EC:
				HAL_GPIO_TogglePin(OP_EC_GPIO_Port, OP_EC_Pin);
				break;
			case SETTING_ENABLE_PH:
				HAL_GPIO_TogglePin(OP_PH_GPIO_Port, OP_PH_Pin);
				break;
			case SETTING_ENABLE_O1:
				HAL_GPIO_TogglePin(OP_O1_GPIO_Port, OP_O1_Pin);
				break;
			case SETTING_ENABLE_O2:
				HAL_GPIO_TogglePin(OP_O2_GPIO_Port, OP_O2_Pin);
				break;
			case SETTING_ENABLE_O3:
				HAL_GPIO_TogglePin(OP_O3_GPIO_Port, OP_O3_Pin);
				break;
			case SETTING_ENABLE_O4:
				HAL_GPIO_TogglePin(OP_O4_GPIO_Port, OP_O4_Pin);
				break;
			case SETTING_ENABLE_O5:
				HAL_GPIO_TogglePin(OP_O5_GPIO_Port, OP_O5_Pin);
				break;
			case SETTING_ENABLE_O6:
				HAL_GPIO_TogglePin(OP_O6_GPIO_Port, OP_O6_Pin);
				break;
			case SETTING_ENABLE_O7:
				HAL_GPIO_TogglePin(OP_O7_GPIO_Port, OP_O7_Pin);
				break;
			case SETTING_ENABLE_O8:
				HAL_GPIO_TogglePin(OP_O8_GPIO_Port, OP_O8_Pin);
				break;
			case SETTING_ENABLE_O9:
				HAL_GPIO_TogglePin(OP_O9_GPIO_Port, OP_O9_Pin);
				break;
			case SETTING_ENABLE_O10:
				HAL_GPIO_TogglePin(OP_O10_GPIO_Port, OP_O10_Pin);
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			modeSetting = SETTING_ENABLE;
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				modeSetting = SETTING_DEFAULT;
				break;
			case SETTING_INFO_FWVERSION:
				break;
			case SETTING_INFO_FARMCODE:
				break;
			}
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}

	return true;
}
bool HandleButtonDown(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		switch (modeDisplay)
		{
		case DISPLAY_EC_PH_WT:
			modeDisplay = DISPLAY_FS_US;
			refreshLcd = true;
			break;
		case DISPLAY_TE_HU_LI:
			modeDisplay = DISPLAY_EC_PH_WT;
			refreshLcd = true;
			break;
		case DISPLAY_FS_US:
			modeDisplay = DISPLAY_TE_HU_LI;
			refreshLcd = true;
			break;
		}
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_INTERNET:
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				modeSetting = SETTING_THRESHOLD;
				break;
			case SETTING_ZIGBEE_STATE:
				if (bZigbeeState == true)
				{
					bZigbeeState = false;
				}
				else
				{
					bZigbeeState = true;
				}
				break;
			case SETTING_ETHERNET_UPDATE:
				if (tFamInfo.nTimeUpload > 1)
					tFamInfo.nTimeUpload--;
				break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				modeSetting = SETTING_PUMP;
				break;
			case SETTING_THRESHOLD_EC:
				tFamInfo.nThrehold_Ec--;
				if (tFamInfo.nThrehold_Ec <= tFamInfo.nThrehold_EcLow){
					tFamInfo.nThrehold_Ec = tFamInfo.nThrehold_EcLow;
				}else if (tFamInfo.nThrehold_Ec >= tFamInfo.nThrehold_ECHigh){
					tFamInfo.nThrehold_Ec = tFamInfo.nThrehold_ECHigh;
				}
				break;
			case SETTING_THRESHOLD_EC_LOW:
				if (tFamInfo.nThrehold_EcLow > 1)
					tFamInfo.nThrehold_EcLow--;
				break;
			case SETTING_THRESHOLD_EC_HIGH:
				if (tFamInfo.nThrehold_ECHigh > tFamInfo.nThrehold_EcLow)
					tFamInfo.nThrehold_ECHigh--;
				break;
			case SETTING_THRESHOLD_EC_DUAL:
				if (tFamInfo.nThrehold_ECDual > 1)
					tFamInfo.nThrehold_ECDual--;
				break;
			case SETTING_THRESHOLD_WATERTEMP:
				tFamInfo.nThrehold_WT -= 10;
				if (tFamInfo.nThrehold_WT <= tFamInfo.nThrehold_WTLow){
					tFamInfo.nThrehold_WT = tFamInfo.nThrehold_WTLow;
				}else if (tFamInfo.nThrehold_WT >= tFamInfo.nThrehold_WTHigh){
					tFamInfo.nThrehold_WT = tFamInfo.nThrehold_WTHigh;
				}
				break;
			case SETTING_THRESHOLD_WATERTEMP_LOW:
				if (tFamInfo.nThrehold_WTLow > 1)
					tFamInfo.nThrehold_WTLow -= 10;
				break;
			case SETTING_THRESHOLD_WATERTEMP_HIGH:
				if (tFamInfo.nThrehold_WTHigh > tFamInfo.nThrehold_WTLow)
					tFamInfo.nThrehold_WTHigh -= 10;
				break;
			case SETTING_THRESHOLD_WATERTEMP_DUAL:
				if (tFamInfo.nThrehold_WTDual > 1)
					tFamInfo.nThrehold_WTDual -= 10;
				break;
			case SETTING_THRESHOLD_PH:
				tFamInfo.nThrehold_PH--;
				if (tFamInfo.nThrehold_PH <= tFamInfo.nThrehold_PHLow){
					tFamInfo.nThrehold_PH = tFamInfo.nThrehold_PHLow;
				}else if (tFamInfo.nThrehold_PH >= tFamInfo.nThrehold_PHHigh){
					tFamInfo.nThrehold_PH = tFamInfo.nThrehold_PHHigh;
				}
				break;
			case SETTING_THRESHOLD_PH_LOW:
				if (tFamInfo.nThrehold_PHLow > 1)
					tFamInfo.nThrehold_PHLow--;
				break;
			case SETTING_THRESHOLD_PH_HIGH:
				if (tFamInfo.nThrehold_PHHigh > tFamInfo.nThrehold_PHLow)
					tFamInfo.nThrehold_PHHigh--;
				break;
			case SETTING_THRESHOLD_PH_DUAL:
				if (tFamInfo.nThrehold_PHDual > 1)
					tFamInfo.nThrehold_PHDual--;
				break;
			case SETTING_THRESHOLD_TE:
				tFamInfo.nThrehold_Cli_Temp -= 10;
				if (tFamInfo.nThrehold_Cli_Temp <= tFamInfo.nThrehold_Cli_TempLow){
					tFamInfo.nThrehold_Cli_Temp = tFamInfo.nThrehold_Cli_TempLow;
				}else if (tFamInfo.nThrehold_Cli_Temp >= tFamInfo.nThrehold_Cli_TempHigh){
					tFamInfo.nThrehold_Cli_Temp = tFamInfo.nThrehold_Cli_TempHigh;
				}
				break;
			case SETTING_THRESHOLD_TE_LOW:
				if (tFamInfo.nThrehold_Cli_TempLow > 1)
					tFamInfo.nThrehold_Cli_TempLow -= 10;
				break;
			case SETTING_THRESHOLD_TE_HIGH:
				if (tFamInfo.nThrehold_Cli_TempHigh > tFamInfo.nThrehold_Cli_TempLow)
					tFamInfo.nThrehold_Cli_TempHigh -= 10;
				break;
			case SETTING_THRESHOLD_TE_DUAL:
				if (tFamInfo.nThrehold_Cli_TempDual > 1)
					tFamInfo.nThrehold_Cli_TempDual -= 10;
				break;
			case SETTING_THRESHOLD_HU:
				tFamInfo.nThrehold_Cli_Humi -= 10;
				if (tFamInfo.nThrehold_Cli_Humi <= tFamInfo.nThrehold_Cli_HumiLow){
					tFamInfo.nThrehold_Cli_Humi = tFamInfo.nThrehold_Cli_HumiLow;
				}else if (tFamInfo.nThrehold_Cli_Humi >= tFamInfo.nThrehold_Cli_HumiHigh){
					tFamInfo.nThrehold_Cli_Humi = tFamInfo.nThrehold_Cli_HumiHigh;
				}
				break;
			case SETTING_THRESHOLD_HU_LOW:
				if (tFamInfo.nThrehold_Cli_HumiLow > 1)
					tFamInfo.nThrehold_Cli_HumiLow -= 10;
				break;
			case SETTING_THRESHOLD_HU_HIGH:
				if (tFamInfo.nThrehold_Cli_HumiHigh > tFamInfo.nThrehold_Cli_HumiLow)
					tFamInfo.nThrehold_Cli_HumiHigh -= 10;
				break;
			case SETTING_THRESHOLD_HU_DUAL:
				if (tFamInfo.nThrehold_Cli_HumiDual > 1)
					tFamInfo.nThrehold_Cli_HumiDual -= 10;
				break;
			case SETTING_THRESHOLD_LI:
				tFamInfo.nThrehold_Cli_Light -= 1000;
				if (tFamInfo.nThrehold_Cli_Light <= tFamInfo.nThrehold_Cli_LightLow){
					tFamInfo.nThrehold_Cli_Light = tFamInfo.nThrehold_Cli_LightLow;
				}else if (tFamInfo.nThrehold_Cli_Light >= tFamInfo.nThrehold_Cli_LightHigh){
					tFamInfo.nThrehold_Cli_Light = tFamInfo.nThrehold_Cli_LightHigh;
				}
				break;
			case SETTING_THRESHOLD_LI_LOW:
				if (tFamInfo.nThrehold_Cli_LightLow > 1)
					tFamInfo.nThrehold_Cli_LightLow -= 1000;
				break;
			case SETTING_THRESHOLD_LI_HIGH:
				if (tFamInfo.nThrehold_Cli_LightHigh > tFamInfo.nThrehold_Cli_LightLow)
					tFamInfo.nThrehold_Cli_LightHigh -= 1000;
				break;
			case SETTING_THRESHOLD_LI_DUAL:
				if (tFamInfo.nThrehold_Cli_LightDual > 1)
					tFamInfo.nThrehold_Cli_LightDual -= 1000;
				break;
			case SETTING_THRESHOLD_FS:
				if (tFamInfo.nThrehold_FlowRate > 1)
					tFamInfo.nThrehold_FlowRate--;
				break;
			case SETTING_THRESHOLD_FS_LOW:
				if (tFamInfo.nThrehold_FlowRateLow > 1)
					tFamInfo.nThrehold_FlowRateLow--;
				break;
			case SETTING_THRESHOLD_FS_HIGH:
				if (tFamInfo.nThrehold_FlowRateHigh > 1)
					tFamInfo.nThrehold_FlowRateHigh--;
				break;
			case SETTING_THRESHOLD_FS_DUAL:
				if (tFamInfo.nThrehold_FlowRateDual > 1)
					tFamInfo.nThrehold_FlowRateDual--;
				break;
			case SETTING_THRESHOLD_TANKLEVEL:
				if (tFamInfo.nThrehold_TankLevel > 1)
					tFamInfo.nThrehold_TankLevel -= 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_LOW:
				if (tFamInfo.nThrehold_TankLevelLow > 1)
					tFamInfo.nThrehold_TankLevelLow -= 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_HIGH:
				if (tFamInfo.nThrehold_TankLevelHigh > 1)
					tFamInfo.nThrehold_TankLevelHigh -= 10;
				break;
			case SETTING_THRESHOLD_TANKLEVEL_DUAL:
				if (tFamInfo.nThrehold_TankLevelDual > 1)
					tFamInfo.nThrehold_TankLevelDual -= 10;
				break;
			}
			break;
		case SETTING_PUMP:
			switch (settingPump)
			{
			case SETTING_PUMP_NONE:
				modeSetting = SETTING_CALIB_EC;
				break;
			case SETTING_TIME_PUMP_PERI:
				if (tFamInfo.nPump_TimePeri > 1)
					tFamInfo.nPump_TimePeri--;
				break;
			case SETTING_TIME_PUMP_MIX:
				if (tFamInfo.nPump_TimePumpMix > 1)
					tFamInfo.nPump_TimePumpMix--;
				break;
			case SETTING_TIME_PUMP_NOZZLE_WAIT:
				if (tFamInfo.nPump_TimeNozzleOff > 1)
					tFamInfo.nPump_TimeNozzleOff--;
				break;
			case SETTING_TIME_PUMP_NOZZLE:
				if (tFamInfo.nPump_TimeNozzleOn > 1)
					tFamInfo.nPump_TimeNozzleOn--;
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				modeSetting = SETTING_CALIB_WT;
				break;
			case SETTING_EC_STANDARD:
				tFamInfo.nStandard_Ec--;
				if (tFamInfo.nStandard_Ec <= 0){
					tFamInfo.nStandard_Ec = 0;
				}else if (tFamInfo.nStandard_Ec >= 5000){
					tFamInfo.nStandard_Ec = 5000;
				}
//				switch (tFamInfo.nStandard_Ec)
//				{
//				case 1000:
//					tFamInfo.nStandard_Ec = 5000;
//					break;
//				case 1413:
//					tFamInfo.nStandard_Ec = 1000;
//					break;
//				case 2000:
//					tFamInfo.nStandard_Ec = 1413;
//					break;
//				case 5000:
//					tFamInfo.nStandard_Ec = 2000;
//					break;
//				default:
//					break;
//				}
				break;
			case SETTING_CLICK_CALIB_EC:
				SENSOR_CalibEc(tFamInfo.nStandard_Ec, &tFamInfo.nCalib_Ec1A, &tFamInfo.nCalib_Ec2A);
				break;
			case SETTING_CALIB_EC1_A:
				if (tFamInfo.nCalib_Ec1A > 1)
					tFamInfo.nCalib_Ec1A--;
				break;
			case SETTING_CALIB_EC1_B:
				if (tFamInfo.nCalib_Ec1B > 1)
					tFamInfo.nCalib_Ec1B--;
				break;
			case SETTING_CALIB_EC2_A:
				if (tFamInfo.nCalib_Ec2A > 1)
					tFamInfo.nCalib_Ec2A--;
				break;
			case SETTING_CALIB_EC2_B:
				if (tFamInfo.nCalib_Ec2B > 1)
					tFamInfo.nCalib_Ec2B--;
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				modeSetting = SETTING_CALIB_PH;
				break;
			case SETTING_WT_STANDARD:
				fWaterTempStandard -= 0.1;
				break;
			case SETTING_CLICK_CALIB_WT:
				SENSOR_CalibWt(fWaterTempStandard);
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				modeSetting = SETTING_CALIB_TE;
				refreshLcd = true;
				break;
			case SETTING_PH_STANDARD:
				switch (tFamInfo.nStandard_Ph)
				{
				case 401:
					tFamInfo.nStandard_Ph = 701;
					break;
				case 701:
					tFamInfo.nStandard_Ph = 401;
					break;
				default:
					break;
				}
				break;
			case SETTING_CLICK_CALIB_PH:
				SENSOR_CalibPh(tFamInfo.nStandard_Ph, &tFamInfo.nCalib_Ph1A, &tFamInfo.nCalib_Ph1B, &tFamInfo.nCalib_Ph2A, &tFamInfo.nCalib_Ph2B);
				break;
			case SETTING_CALIB_PH1_A:
				if (tFamInfo.nCalib_Ph1A > 1)
					tFamInfo.nCalib_Ph1A--;
				break;
			case SETTING_CALIB_PH1_B:
				if (tFamInfo.nCalib_Ph1B > 1)
					tFamInfo.nCalib_Ph1B--;
				break;
			case SETTING_CALIB_PH2_A:
				if (tFamInfo.nCalib_Ph2A > 1)
					tFamInfo.nCalib_Ph2A--;
				break;
			case SETTING_CALIB_PH2_B:
				if (tFamInfo.nCalib_Ph2B < 1)
					tFamInfo.nCalib_Ph2B--;
			default:
				break;
			}
			break;
		case SETTING_CALIB_TE:
			switch (settingCalibTE)
			{
			case SETTING_CALIB_TE_NONE:
				modeSetting = SETTING_CALIB_HU;
				break;
			case SETTING_CALIB_TE_STAND:
				fClimateTempStandard -= 0.1;
				SENSOR_CalibTemp(fClimateTempStandard);
				break;
			}
			break;
		case SETTING_CALIB_HU:
			switch (settingCalibHU)
			{
			case SETTING_CALIB_HU_NONE:
				modeSetting = SETTING_CALIB_LI;
				break;
			case SETTING_CALIB_HU_STAND:
				fClimateHumiStandard -= 1;
				SENSOR_CalibHumi(fClimateHumiStandard);
				break;
			}
			break;
		case SETTING_CALIB_LI:
			switch (settingCalibLI)
			{
			case SETTING_CALIB_LI_NONE:
				modeSetting = SETTING_CALIB_FS;
				refreshLcd = true;
				break;
			case SETTING_CALIB_LI_STAND:
				nClimateLightStandard -= 100;
				SENSOR_CalibLight(nClimateLightStandard);
				break;
			}
			break;
		case SETTING_CALIB_FS:
			switch (settingCalibFS)
			{
			case SETTING_CALIB_FS_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_1;
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_1:
			switch (settingCalibTankLevel1)
			{
			case SETTING_CALIB_TANK_LEVEL_1_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_2;
				break;
			case SETTING_CALIB_TANK_LEVEL_1_STAND:
				nTankLevelStandard1 -= 1;
				HCSR04_calibTank1(nTankLevelStandard1);
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_2:
			switch (settingCalibTankLevel2)
			{
			case SETTING_CALIB_TANK_LEVEL_2_NONE:
				modeSetting = SETTING_CALIB_TANK_LEVEL_3;
				refreshLcd = true;
				break;
			case SETTING_CALIB_TANK_LEVEL_2_STAND:
				nTankLevelStandard2 -= 1;
				HCSR04_calibTank2(nTankLevelStandard2);
				break;
			}
			break;
		case SETTING_CALIB_TANK_LEVEL_3:
			switch (settingCalibTankLevel3)
			{
			case SETTING_CALIB_TANK_LEVEL_3_NONE:
				modeSetting = SETTING_EXIO;
				break;
			case SETTING_CALIB_TANK_LEVEL_3_STAND:
				nTankLevelStandard3 -= 1;
				HCSR04_calibTank3(nTankLevelStandard3);
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingExIO)
			{
			case SETTING_EXIO_NONE:
				modeSetting = SETTING_RTC;
				break;
			case SETTING_EXIO_NOZZLE_TIME_ON:
				tFamInfo.nTime_NozzleOn--;
				if (tFamInfo.nTime_NozzleOn >= 12)
					tFamInfo.nTime_NozzleOn = 12;
				else if (tFamInfo.nTime_NozzleOn <= 0)
					tFamInfo.nTime_NozzleOn = 0;
				break;
			case SETTING_EXIO_NOZZLE_TIME_OFF:
				tFamInfo.nTime_NozzleOff--;
				if (tFamInfo.nTime_NozzleOff >= 23)
					tFamInfo.nTime_NozzleOff = 23;
				else if (tFamInfo.nTime_NozzleOff <= 12)
					tFamInfo.nTime_NozzleOff = 12;
				break;
			case SETTING_EXIO_COOPAD_TIME_ON:
				tFamInfo.nTime_CoopadOn--;
				if (tFamInfo.nTime_CoopadOn >= 12)
					tFamInfo.nTime_CoopadOn = 12;
				else if (tFamInfo.nTime_CoopadOn <= 0)
					tFamInfo.nTime_CoopadOn = 0;
				break;
			case SETTING_EXIO_COOPAD_TIME_OFF:
				tFamInfo.nTime_CoopadOff--;
				if (tFamInfo.nTime_CoopadOff >= 23)
					tFamInfo.nTime_CoopadOff = 23;
				else if (tFamInfo.nTime_CoopadOff <= 12)
					tFamInfo.nTime_CoopadOff = 12;
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				modeSetting = SETTING_ENABLE;
				refreshLcd = true;
				break;
			case SETTING_RTC_HOUR:
				//Increase Hour
				tRtc.Hour--;
				if (tRtc.Hour > 23)
					tRtc.Hour = 0;
				else if (tRtc.Hour < 0)
					tRtc.Hour = 23;
				break;
			case SETTING_RTC_MINUTE:
				//Increase Minute
				tRtc.Min--;
				if (tRtc.Min > 59)
					tRtc.Min = 0;
				else if (tRtc.Min < 0)
					tRtc.Min = 59;
				break;
			}
			break;
		case SETTING_ENABLE:
			switch (settingEnable)
			{
			case SETTING_ENABLE_NONE:
				modeSetting = SETTING_DEFAULT;
				break;
			case SETTING_ENABLE_EC:
				HAL_GPIO_TogglePin(OP_EC_GPIO_Port, OP_EC_Pin);
				break;
			case SETTING_ENABLE_PH:
				HAL_GPIO_TogglePin(OP_PH_GPIO_Port, OP_PH_Pin);
				break;
			case SETTING_ENABLE_O1:
				HAL_GPIO_TogglePin(OP_O1_GPIO_Port, OP_O1_Pin);
				break;
			case SETTING_ENABLE_O2:
				HAL_GPIO_TogglePin(OP_O2_GPIO_Port, OP_O2_Pin);
				break;
			case SETTING_ENABLE_O3:
				HAL_GPIO_TogglePin(OP_O3_GPIO_Port, OP_O3_Pin);
				break;
			case SETTING_ENABLE_O4:
				HAL_GPIO_TogglePin(OP_O4_GPIO_Port, OP_O4_Pin);
				break;
			case SETTING_ENABLE_O5:
				HAL_GPIO_TogglePin(OP_O5_GPIO_Port, OP_O5_Pin);
				break;
			case SETTING_ENABLE_O6:
				HAL_GPIO_TogglePin(OP_O6_GPIO_Port, OP_O6_Pin);
				break;
			case SETTING_ENABLE_O7:
				HAL_GPIO_TogglePin(OP_O7_GPIO_Port, OP_O7_Pin);
				break;
			case SETTING_ENABLE_O8:
				HAL_GPIO_TogglePin(OP_O8_GPIO_Port, OP_O8_Pin);
				break;
			case SETTING_ENABLE_O9:
				HAL_GPIO_TogglePin(OP_O9_GPIO_Port, OP_O9_Pin);
				break;
			case SETTING_ENABLE_O10:
				HAL_GPIO_TogglePin(OP_O10_GPIO_Port, OP_O10_Pin);
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			modeSetting = SETTING_INFO;
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				modeSetting = SETTING_INTERNET;
				refreshLcd = true;
				break;
			case SETTING_INFO_FWVERSION:
				break;
			case SETTING_INFO_FARMCODE:
				break;
			}
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:

		break;
	}

	return true;
}
