/*
 * hcsr04.h
 *
 *  Created on: 2020. 8. 21.
 *      Author: Cuong Le Hung
 */
#ifndef __HCSR04_H
#define __HCSR04_H
 
#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <stdbool.h>

typedef enum
{
	CHECK_TANK1_OK = 30,
	CHECK_TANK1_DUAL,
	CHECK_TANK1_HIGH,
	CHECK_TANK1_LOW
} CheckTank1_t;

typedef enum
{
	CHECK_TANK2_OK = 30,
	CHECK_TANK2_DUAL,
	CHECK_TANK2_HIGH,
	CHECK_TANK2_LOW
} CheckTank2_t;

typedef enum
{
	CHECK_TANK3_OK = 30,
	CHECK_TANK3_DUAL,
	CHECK_TANK3_HIGH,
	CHECK_TANK3_LOW
} CheckTank3_t;

typedef enum
{
	STATE_US1 = 1,
	STATE_US2,
	STATE_US3,
	STATE_US4,
	STATE_US5,
	STATE_US6
} STATE_US;

typedef struct
{
	uint16_t dDisUs1;
	uint16_t dDisUs2;
	uint16_t dDisUs3;
	uint16_t dDisUs4;
	uint16_t dDisUs5;
	uint16_t dDisUs6;
	uint16_t dLitUs1;
	uint16_t dLitUs2;
	uint16_t dLitUs3;
	uint16_t dLitUs4;
	uint16_t dLitUs5;
	uint16_t dLitUs6;
	uint16_t dLitTank1;
	uint16_t dLitTank2;
	uint16_t dLitTank3;
	CheckTank1_t tTank1Check;
	CheckTank2_t tTank2Check;
	CheckTank3_t tTank3Check;
} _HCSR04;

void HCSR04_Init(TIM_HandleTypeDef *tim);
bool HCSR04_getDistance(void);
bool HCSR04_calibTank1(uint16_t avg);
bool HCSR04_calibTank2(uint16_t avg);
bool HCSR04_calibTank3(uint16_t avg);
bool HCSR04_Timer(void);

#endif
