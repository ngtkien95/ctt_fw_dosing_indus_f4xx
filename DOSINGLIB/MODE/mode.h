/*
 * mode.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __MODE_H
#define __MODE_H

#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <stdbool.h>

typedef struct
{
	char cFarmCode[20];
	char cFarmName[12];
	char cFwVersion[10];
	char cDateTime[30];
	
	int32_t nDateTime_Hour;
	int32_t nDateTime_Minute;
	int32_t nTimeUpload;
	int32_t nThrehold_Ec;
	int32_t nThrehold_EcLow;
	int32_t nThrehold_ECHigh;
	int32_t nThrehold_ECDual;
	int32_t nThrehold_WT;
	int32_t nThrehold_WTLow;
	int32_t nThrehold_WTHigh;
	int32_t nThrehold_WTDual;
	int32_t nThrehold_PH;
	int32_t nThrehold_PHLow;
	int32_t nThrehold_PHHigh;
	int32_t nThrehold_PHDual;
	int32_t nThrehold_Cli_Temp;
	int32_t nThrehold_Cli_TempLow;
	int32_t nThrehold_Cli_TempHigh;
	int32_t nThrehold_Cli_TempDual;
	int32_t nThrehold_Cli_Humi;
	int32_t nThrehold_Cli_HumiLow;
	int32_t nThrehold_Cli_HumiHigh;
	int32_t nThrehold_Cli_HumiDual;
	int32_t nThrehold_Cli_Light;
	int32_t nThrehold_Cli_LightLow;
	int32_t nThrehold_Cli_LightHigh;
	int32_t nThrehold_Cli_LightDual;
	int32_t nThrehold_FlowRate;
	int32_t nThrehold_FlowRateLow;
	int32_t nThrehold_FlowRateHigh;
	int32_t nThrehold_FlowRateDual;
	int32_t nThrehold_TankLevel;
	int32_t nThrehold_TankLevelLow;
	int32_t nThrehold_TankLevelHigh;
	int32_t nThrehold_TankLevelDual;
	int32_t nPump_TimePeri;
	int32_t nPump_TimeNozzleOn;
	int32_t nPump_TimeNozzleOff;
	int32_t nPump_CooPadOn;
	int32_t nPump_CooPadOff;
	int32_t nPump_ChillerOn;
	int32_t nPump_ChillerOff;
	int32_t nPump_TimePumpMix;
	int32_t nCalib_Ec1A;
	int32_t nCalib_Ec1B;
	int32_t nCalib_Ec2A;
	int32_t nCalib_Ec2B;
	int32_t nCalib_Wt1A;
	int32_t nCalib_Wt1B;
	int32_t nCalib_Wt2A;
	int32_t nCalib_Wt2B;
	int32_t nCalib_Ph1A;
	int32_t nCalib_Ph1B;
	int32_t nCalib_Ph2A;
	int32_t nCalib_Ph2B;
	int32_t nOffset_Cli_Temp1;
	int32_t nOffset_Cli_Temp2;
	int32_t nOffset_Cli_Humi1;
	int32_t nOffset_Cli_Humi2;
	int32_t nOffset_Cli_Light1;
	int32_t nOffset_Cli_Light2;
	int32_t nOffset_US1;
	int32_t nOffset_US2;
	int32_t nOffset_US3;
	int32_t nOffset_US4;
	int32_t nOffset_US5;
	int32_t nOffset_US6;
	int32_t nVoltage_Ph1_Newtral;
	int32_t nVoltage_Ph1_Acid;
	int32_t nVoltage_Ph2_Newtral;
	int32_t nVoltage_Ph2_Acid;
	int32_t nStandard_Ec;
	int32_t nStandard_Ph;
	int32_t nTime_NozzleOn;
	int32_t nTime_NozzleOff;
	int32_t nTime_CoopadOn;
	int32_t nTime_CoopadOff;
	int32_t nTime_ChillerOn;
	int32_t nTime_ChillerOff;
	int32_t nTime_ShadingNetOn;
	int32_t nTime_ShadingNetOff;
} _FAM_INFO;

typedef enum
{
	MODE_DISPLAY = 10,
	MODE_SETTING,
	MODE_CONFIG_SUCCESS
} flagMode_t;
typedef enum
{
	DISPLAY_EC_PH_WT = 13,
	DISPLAY_TE_HU_LI,
	DISPLAY_FS_US
} modeDisplay_t;
typedef enum
{
	SETTING_INTERNET = 1,
	SETTING_THRESHOLD,
	SETTING_PUMP,
	SETTING_CALIB_EC,
	SETTING_CALIB_WT,
	SETTING_CALIB_PH,
	SETTING_CALIB_TE,
	SETTING_CALIB_HU,
	SETTING_CALIB_LI,
	SETTING_CALIB_FS,
	SETTING_CALIB_TANK_LEVEL_1,
	SETTING_CALIB_TANK_LEVEL_2,
	SETTING_CALIB_TANK_LEVEL_3,
	SETTING_EXIO,
	SETTING_RTC,
	SETTING_ENABLE,
	SETTING_DEFAULT,
	SETTING_INFO
} modeSetting_t;
typedef enum
{
	SETTING_RTC_NONE = 24,
	SETTING_RTC_HOUR,
	SETTING_RTC_MINUTE
} settingRtc_t;
typedef enum
{
	SETTING_ETHERNET_NONE = 30,
	SETTING_ZIGBEE_STATE,
	SETTING_ETHERNET_UPDATE
} settingEthernet_t;
typedef enum
{
	SETTING_THRESHOLD_NONE = 149,
	SETTING_THRESHOLD_EC,
	SETTING_THRESHOLD_EC_LOW,
	SETTING_THRESHOLD_EC_HIGH,
	SETTING_THRESHOLD_EC_DUAL,
	SETTING_THRESHOLD_WATERTEMP,
	SETTING_THRESHOLD_WATERTEMP_LOW,
	SETTING_THRESHOLD_WATERTEMP_HIGH,
	SETTING_THRESHOLD_WATERTEMP_DUAL,
	SETTING_THRESHOLD_PH,
	SETTING_THRESHOLD_PH_LOW,
	SETTING_THRESHOLD_PH_HIGH,
	SETTING_THRESHOLD_PH_DUAL,
	SETTING_THRESHOLD_TE,
	SETTING_THRESHOLD_TE_LOW,
	SETTING_THRESHOLD_TE_HIGH,
	SETTING_THRESHOLD_TE_DUAL,
	SETTING_THRESHOLD_HU,
	SETTING_THRESHOLD_HU_LOW,
	SETTING_THRESHOLD_HU_HIGH,
	SETTING_THRESHOLD_HU_DUAL,
	SETTING_THRESHOLD_LI,
	SETTING_THRESHOLD_LI_LOW,
	SETTING_THRESHOLD_LI_HIGH,
	SETTING_THRESHOLD_LI_DUAL,
	SETTING_THRESHOLD_FS,
	SETTING_THRESHOLD_FS_LOW,
	SETTING_THRESHOLD_FS_HIGH,
	SETTING_THRESHOLD_FS_DUAL,
	SETTING_THRESHOLD_TANKLEVEL,
	SETTING_THRESHOLD_TANKLEVEL_LOW,
	SETTING_THRESHOLD_TANKLEVEL_HIGH,
	SETTING_THRESHOLD_TANKLEVEL_DUAL
} settingThreshold_t;
typedef enum
{
	SETTING_PUMP_NONE = 200,
	SETTING_TIME_PUMP_PERI,
	SETTING_TIME_PUMP_MIX,
	SETTING_TIME_PUMP_NOZZLE,
	SETTING_TIME_PUMP_NOZZLE_WAIT,
} settingPump_t;
typedef enum
{
	SETTING_CALIB_EC_NONE = 210,
	SETTING_CALIB_EC1_A,
	SETTING_CALIB_EC1_B,
	SETTING_EC_STANDARD,
	SETTING_CLICK_CALIB_EC,
	SETTING_CALIB_EC2_A,
	SETTING_CALIB_EC2_B
} settingCalibEC_t;
typedef enum
{
	SETTING_CALIB_WT_NONE = 200,
	SETTING_WT_STANDARD,
	SETTING_CLICK_CALIB_WT
} settingCalibWT_t;
typedef enum
{
	SETTING_CALIB_PH_NONE = 50,
	SETTING_PH_STANDARD = 51,
	SETTING_CLICK_CALIB_PH = 52,
	SETTING_CALIB_PH1_A = 53,
	SETTING_CALIB_PH1_B = 54,
	SETTING_CALIB_PH2_A = 55,
	SETTING_CALIB_PH2_B = 56,
} settingCalibPH_t;
typedef enum
{
	SETTING_CALIB_TE_NONE = 215,
	SETTING_CALIB_TE_STAND
} settingCalibTE_t;
typedef enum
{
	SETTING_CALIB_HU_NONE = 220,
	SETTING_CALIB_HU_STAND
} settingCalibHU_t;
typedef enum
{
	SETTING_CALIB_LI_NONE = 40,
	SETTING_CALIB_LI_STAND
} settingCalibLI_t;
typedef enum
{
	SETTING_CALIB_FS_NONE = 45,
	SETTING_CALIB_FS_STAND
} settingCalibFS_t;
typedef enum
{
	SETTING_CALIB_TANK_LEVEL_1_NONE = 50,
	SETTING_CALIB_TANK_LEVEL_1_STAND
} settingCalibTankLevel1_t;
typedef enum
{
	SETTING_CALIB_TANK_LEVEL_2_NONE = 55,
	SETTING_CALIB_TANK_LEVEL_2_STAND
} settingCalibTankLevel2_t;
typedef enum
{
	SETTING_CALIB_TANK_LEVEL_3_NONE = 60,
	SETTING_CALIB_TANK_LEVEL_3_STAND
} settingCalibTankLevel3_t;
typedef enum
{
	SETTING_EXIO_NONE = 230,
	SETTING_EXIO_NOZZLE_TIME_ON,
	SETTING_EXIO_NOZZLE_TIME_OFF,
	SETTING_EXIO_COOPAD_TIME_ON,
	SETTING_EXIO_COOPAD_TIME_OFF
} settingExIO_t;
typedef enum
{
	SETTING_ENABLE_NONE = 240,
	SETTING_ENABLE_EC = 241,
	SETTING_ENABLE_PH = 242,
	SETTING_ENABLE_O1 = 243,
	SETTING_ENABLE_O2 = 244,
	SETTING_ENABLE_O3 = 245,
	SETTING_ENABLE_O4 = 246,
	SETTING_ENABLE_O5 = 247,
	SETTING_ENABLE_O6 = 248,
	SETTING_ENABLE_O7 = 249,
	SETTING_ENABLE_O8 = 250,
	SETTING_ENABLE_O9 = 251,
	SETTING_ENABLE_O10 = 252
} settingEnable_t;
typedef enum
{
	SETTING_INFO_NONE = 243,
	SETTING_INFO_FWVERSION = 241,
	SETTING_INFO_FARMCODE = 242
} settingInfo_t;

/* Function Prototype */
void MODE_Init(void);
bool MODE_Lcd(void);
bool MODE_Timer(void);
void MODE_Disp_Success(void);
//bool MODE_ParamInit(void);
//bool MODE_ParamSave(void);
bool MODE_ParamsServerUpdate(void);
bool MODE_ParamsServerSave(void);
bool MODE_ParamsServerLoad(void);
void MODE_ConfigParameterBySoft(void);


bool HandleButtonMode(void);
bool HandleButtonSet(void);
bool HandleButtonUp(void);
bool HandleButtonDown(void);



#endif
