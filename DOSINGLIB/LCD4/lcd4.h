/*
 * lcd4.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __LCD4_H
#define __LCD4_H

#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <stdbool.h>

#define HD44780_SETCGRAMADDR 	((uint8_t)0x40U)

#define PIN_HIGH(PORT,PIN)		HAL_GPIO_WritePin(PORT,PIN,GPIO_PIN_SET);
#define PIN_LOW(PORT,PIN)			HAL_GPIO_WritePin(PORT,PIN,GPIO_PIN_RESET);
//---------DECLARE------------------
//#define LCD16xN //For LCD 16xN
#define LCD20xN //For LCD 20xN

//---------FUNCTION PROTOTYPE-------
void LCD4_Init(void);
void LCD4_WriteCursor(uint8_t x, uint8_t y, int8_t *string);
bool LCD4_LoadCustomChar(uint8_t cell, uint8_t *charMap);
void LCD4_WriteCommand(uint8_t type,uint8_t data);
void LCD4_Clear(void);

#endif
