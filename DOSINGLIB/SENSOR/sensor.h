/*
 * sensor.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __SENSOR_H
#define __SENSOR_H

#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <stdbool.h>

//#define VOL_AMP_3V3

typedef enum
{
	CHECK_EC_OK = 1,
	CHECK_EC_HIGH = 2,
	CHECK_EC_LOW = 3,
	CHECK_EC_DUAL = 4
} sysEcCheck_t;

typedef enum
{
	CHECK_WT_OK = 5,
	CHECK_WT_HIGH = 6,
	CHECK_WT_LOW = 7,
	CHECK_WT_DUAL = 8
} sysWtCheck_t;

typedef enum
{
	CHECK_PH_OK = 9,
	CHECK_PH_HIGH = 10,
	CHECK_PH_LOW = 11,
	CHECK_PH_DUAL = 12
} sysPhCheck_t;

typedef struct
{
	double dEc1;
	double dEc1mv;
	double dEc2;
	double dEc2mv;
	double dWaterTemp1;
	double dWaterTemp1mv;
	double dWaterTemp2;
	double dWaterTemp2mv;
	double dPh1;
	double dPh1mv;
	double dPh2;
	double dPh2mv;
	double dEcAvg;
	double dPhAvg;
	double dWtAvg;
	sysEcCheck_t tEcCheck;
	sysWtCheck_t tWtCheck;
	sysPhCheck_t tPhCheck;
} _SENSOR_IRRI;

//typedef struct
//{
//	int16_t CALIB_EC1_A;
//	int16_t CALIB_EC1_B;
////	int16_t CALIB_WT1_A;
////	int16_t CALIB_WT1_B;
//	int16_t OFFSET_CALIB_WT1;
//	int16_t CALIB_EC2_A;
//	int16_t CALIB_EC2_B;
////	int16_t CALIB_WT2_A;
////	int16_t CALIB_WT2_B;
//	int16_t OFFSET_CALIB_WT2;
//	int16_t CALIB_PH1_A;
//	int16_t CALIB_PH1_B;
//	int16_t CALIB_PH2_A;
//	int16_t CALIB_PH2_B;
//	uint16_t STANDARD_EC;
//	uint16_t STANDARD_PH;
//	int16_t NEUTRAL_PH1_VOLTAGE;
//	int16_t NEUTRAL_PH2_VOLTAGE;
//	int16_t ACID_PH1_VOLTAGE;
//	int16_t ACID_PH2_VOLTAGE;

//	// parameter for calib climate temp/humi/light
//	int16_t OFFSET_CLIMATE_TEMP1;
//	int16_t OFFSET_CLIMATE_TEMP2;
//	int16_t OFFSET_CLIMATE_HUMI1;
//	int16_t OFFSET_CLIMATE_HUMI2;
//	int16_t OFFSET_CLIMATE_LIGHT1;
//	int16_t OFFSET_CLIMATE_LIGHT2;

//	// parameter for calib tank level
//	int16_t OFFSET_US_1;
//	int16_t OFFSET_US_2;
//	int16_t OFFSET_US_3;
//	int16_t OFFSET_US_4;
//	int16_t OFFSET_US_5;
//	int16_t OFFSET_US_6;
//} _SENSOR_PARAM;

//typedef struct
//{
//	int16_t _nEc;
//	int16_t _nEcLow;
//	int16_t _nEcDual;
//	int16_t _nWTemp;
//	int16_t _nPh;
//	int16_t _nPhHigh;
//	int16_t _nPhDual;
//	int16_t _nCTemp;
//	int16_t _nCHumi;
//	int16_t _nCLight;
//	int16_t _nFlow;
//	int16_t _nUltraSonic1;
//	int16_t _nUltraSonic2;
//	int16_t _nUltraSonic3;
//} _SENSOR_THREHOLD;

void SENSOR_IrriInit(ADC_HandleTypeDef *handle);
void SENSOR_IrriInit2(ADC_HandleTypeDef *handle1, ADC_HandleTypeDef *handle2);
bool SENSOR_IrriRead(void);
bool SENSOR_IrriCheck(void);
bool SENSOR_CalibEc(int32_t stand, int32_t *retEc1, int32_t *retEc2);
bool SENSOR_CalibPh(int32_t stand, int32_t *ph1A, int32_t *ph1B, int32_t *ph2A, int32_t *ph2B);
bool SENSOR_CalibWt(volatile float offset);
//bool SENSOR_IrriGetValue(_SENSOR_IRRI *irriValue);
//bool SENSOR_IrriGetParam(_SENSOR_PARAM *temp);
//bool SENSOR_IrriSetParam(_SENSOR_PARAM *temp);
//bool SENSOR_IrriGetThrehold(_SENSOR_THREHOLD *temp);
//bool SENSOR_IrriSetThrehold(_SENSOR_THREHOLD *temp);

#endif
