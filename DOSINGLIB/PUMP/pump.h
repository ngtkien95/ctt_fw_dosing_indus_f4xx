/*
 * pump.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __PUMP_H
#define __PUMP_H

#include "stm32f4xx_hal.h"
#include <stdlib.h>
#include <stdbool.h>

#define SET_ON 1
#define SET_OFF 0
#define SET_PMIX(x) HAL_GPIO_WritePin(OP_O4_GPIO_Port, OP_O4_Pin, x);
#define SET_PUMP_EC(x) HAL_GPIO_WritePin(OP_EC_GPIO_Port, OP_EC_Pin, x);
#define SET_PUMP_PH(x) HAL_GPIO_WritePin(OP_PH_GPIO_Port, OP_PH_Pin, x);

typedef enum
{
	STT_PUMP_SLEEP = 1,
	STT_CHECK_SENSOR,
	STT_CTL_MOTOR_PERI,
	STT_CTL_MOTOR_MIX,
	STT_UPDATE_SERVER
} sttControlPump_t;

//typedef struct
//{
//	int16_t _nTimePumpPeri;
//	int16_t _nTimePumpNozzle;
//	int16_t _nTimePumpNozzleWait;
//	int16_t _nTimeOnMotorMix;
//} _PUMP_THRESHOLD;


void PUMP_Init(uint32_t time);
bool PUMP_Timer1s(void);
bool PUMP_Timer1ms(void);
bool PUMP_Process(void);
bool PUMP_Control(void);

#endif
